profbrony
=========

A Symfony project created on June 3, 2017, 7:59 pm.

**Порядок сборки** 

1) Установить composer обновить зависимости бэкэнда  
    _dev_  
        $ composer install  
    _prod_  
        $ composer install --no-dev --optimize-autoloader
        
2) Установить yarn и обновить зависимости фронтенда - yarn install
3) Сделать миграцию БД -   php bin/console doctrine:migrations:migrate
4) Собрать фронтенд с webpack  
    dev  
        $ ./node_modules/.bin/encore dev  
    prod   
        $ ./node_modules/.bin/encore production
        
5) Обновить кэш -  
    dev -  
        $ php bin/console cache:clear --env=dev --no-warmup  
    prod -  
        $ php bin/console cache:clear --env=prod --no-debug --no-warmup  
        $ php bin/console cache:warmup --env=prod  
        
                 
**Дополнения**
1) Установить sonata admin - https://sonata-project.org/bundles/admin/3-x/doc/getting_started/installation.html 
2) Для работы с файлами Sonata media bundle - https://sonata-project.org/bundles/media/3-x/doc/reference/installation.html
    При первой инсталяции -
        $ mkdir web/uploads
        $ mkdir web/uploads/media
        $ chmod -R 0777 web/uploads  
3) Альтернатива чистки кэша на продакшн - rm -rf var/cache/*