<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rate
 *
 * @ORM\Table(name="rate")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RateRepository")
 */
class Rate
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Developer", inversedBy="rate")
     */
    private $developer;

    /**
     * @var float
     *
     * @ORM\Column(name="percent_profbrony", type="float")
     */
    private $percent_profbrony;

    /**
     * @var float
     *
     * @ORM\Column(name="percent_for_you", type="float")
     */
    private $percent_for_you;

    /**
     * @var float
     *
     * @ORM\Column(name="percent_max", type="float")
     */
    private $percent_max;

    /**
     * @var float
     *
     * @ORM\Column(name="percent_prof_parther", type="float")
     */
    private $percent_prof_parther;

    /**
     * @var string
     *
     * @ORM\Column(name="persent_dop", type="string", length=250)
     */
    private $persent_dop;

    /**
     * @var string
     *
     * @ORM\Column(name="persent_object", type="string", length=255)
     */
    private $persent_object;

    /**
     * @var float
     *
     * @ORM\Column(name="percent_private", type="float")
     */
    private $percent_private;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $developer
     */
    public function setDeveloper($developer)
    {
        $this->developer = $developer;
    }

    /**
     * @return mixed
     */
    public function getDeveloper()
    {
        return $this->developer;
    }

    /**
     * Set percentProfbrony
     *
     * @param float $percentProfbrony
     *
     * @return Rate
     */
    public function setPercentProfbrony($percentProfbrony)
    {
        $this->percent_profbrony = $percentProfbrony;

        return $this;
    }

    /**
     * Get percentProfbrony
     *
     * @return float
     */
    public function getPercentProfbrony()
    {
        return $this->percent_profbrony;
    }

    /**
     * Set percentForYou
     *
     * @param float $percentForYou
     *
     * @return Rate
     */
    public function setPercentForYou($percentForYou)
    {
        $this->percent_for_you = $percentForYou;

        return $this;
    }

    /**
     * Get percentForYou
     *
     * @return float
     */
    public function getPercentForYou()
    {
        return $this->percent_for_you;
    }

    /**
     * Set percentMax
     *
     * @param float $percentMax
     *
     * @return Rate
     */
    public function setPercentMax($percentMax)
    {
        $this->percent_max = $percentMax;

        return $this;
    }

    /**
     * Get percentMax
     *
     * @return float
     */
    public function getPercentMax()
    {
        return $this->percent_max;
    }

    /**
     * Set percentProfParther
     *
     * @param float $percentProfParther
     *
     * @return Rate
     */
    public function setPercentProfParther($percentProfParther)
    {
        $this->percent_prof_parther = $percentProfParther;

        return $this;
    }

    /**
     * Get percentProfParther
     *
     * @return float
     */
    public function getPercentProfParther()
    {
        return $this->percent_prof_parther;
    }

    /**
     * Set persentDop
     *
     * @param string $persentDop
     *
     * @return Rate
     */
    public function setPersentDop($persentDop)
    {
        $this->persent_dop = $persentDop;

        return $this;
    }

    /**
     * Get persentDop
     *
     * @return string
     */
    public function getPersentDop()
    {
        return $this->persent_dop;
    }

    /**
     * Set persent_object
     *
     * @param string $persent_object
     *
     * @return Rate
     */
    public function setPersentObject($persent_object)
    {
        $this->persent_object = $persent_object;

        return $this;
    }

    /**
     * Get persent_object
     *
     * @return string
     */
    public function getPersentObject()
    {
        return $this->persent_object;
    }

    /**
     * Set percentPrivate
     *
     * @param float $percentPrivate
     *
     * @return Rate
     */
    public function setPercentPrivate($percentPrivate)
    {
        $this->percent_private = $percentPrivate;

        return $this;
    }

    /**
     * Get percentPrivate
     *
     * @return float
     */
    public function getPercentPrivate()
    {
        return $this->percent_private;
    }

}

