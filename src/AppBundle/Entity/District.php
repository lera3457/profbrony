<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * District
 *
 * @ORM\Table(name="district")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DistrictRepository")
 */
class District
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\City", inversedBy="districts")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $city;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\SubDistrict", mappedBy="district")
     */
    private $subDistricts;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Street", mappedBy="district")
     */
    private $streets;

    public function __construct()
    {
        $this->streets = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getName() ? : '';
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return District
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getStreets()
    {
        return $this->streets;
    }

    /**
     * @param mixed $streets
     */
    public function setStreets($streets)
    {
        $this->streets = $streets;
    }

    /**
     * @return mixed
     */
    public function getSubDistricts()
    {
        return $this->subDistricts;
    }

    /**
     * @param mixed $subDistricts
     */
    public function setSubDistricts($subDistricts)
    {
        $this->subDistricts = $subDistricts;
    }


}

