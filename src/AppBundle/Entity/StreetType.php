<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * StreetType
 *
 * @ORM\Table(name="street_type")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StreetTypeRepository")
 */
class StreetType
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=40)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="string", length=10)
     */
    private $nameAbbreviated;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Street", mappedBy="type")
     */
    private $street;

    public function __toString()
    {
        return $this->getName() ? : '';
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return StreetType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param mixed $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * @return string
     */
    public function getNameAbbreviated(): string
    {
        return $this->nameAbbreviated;
    }

    /**
     * @param string $nameAbbreviated
     */
    public function setNameAbbreviated(string $nameAbbreviated)
    {
        $this->nameAbbreviated = $nameAbbreviated;
    }

}

