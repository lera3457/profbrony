<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;

/**
 * Building
 *
 * @ORM\Table(name="building")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BuildingRepository")
 */
class Building
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @ORM\Column(name="name", type="string", length=255)
     */
    public $name;

    /**
     * @ORM\Column(type="boolean")
     */
    private $fz_214 = false;

    /**
     *  Рассрочка
     * @ORM\Column(type="boolean")
     */
    private $installment;

    /**
     *  Ипотека
     * @ORM\Column(type="boolean")
     */
    private $mortgage;

    /**
     *  Военная ипотека
     * @ORM\Column(type="boolean")
     */
    private $militaryMortgage;

    /**
     * @ORM\Column(type="date")
     */
    private $dateStart;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $deadlineYear;

    /**
     * @ORM\Column(type="QuarterType", nullable=false)
     * @DoctrineAssert\Enum(entity="AppBundle\DBAL\Types\QuarterType")
     */
    private $deadlineQuarter;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isReady;

    /**
     * @ORM\Column(type="integer")
     */
    private $blockCount = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $floorCount = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $flatCount = 0;

    /**
     * @ORM\Column(type="BuildingMaterialType", nullable=false)
     * @DoctrineAssert\Enum(entity="AppBundle\DBAL\Types\BuildingMaterialType")
     */
    private $materialType;

    /**
     * @ORM\Column(type="BuildingType", nullable=false)
     * @DoctrineAssert\Enum(entity="AppBundle\DBAL\Types\BuildingType")
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Object", inversedBy="buildings")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $object;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Address", inversedBy="building")
     */
    private $address;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Block", mappedBy="building")
     */
    private $blocks;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\FloorLayout", mappedBy="building")
     */
    private $floorLayouts;

    /**
     * Массив для передачи агрегаций поиска
     * @var array
     */
    private $aggs;

    /**
     * @ORM\Column(name="percent_profbrony", type="float")
     */
    public $percent_profbrony;

    /**
     * @ORM\Column(name="percent_for_you", type="float")
     */
    public $percent_for_you;

    /**
     * @ORM\Column(name="percent_max", type="float")
     */
    public $percent_max;

    /**
     * @ORM\Column(name="percent_prof_parther", type="float")
     */
    public $percent_prof_parther;

    /**
     * @ORM\Column(name="percent_dop", type="string")
     */
    public $percent_dop;


    public function __construct()
    {
        $this->blocks = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getName() ? : '';
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Building
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * @param mixed $object
     */
    public function setObject($object)
    {
        $this->object = $object;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getBlocks()
    {
        return $this->blocks;
    }

    /**
     * @param mixed $blocks
     */
    public function setBlocks($blocks)
    {
        $this->blocks = $blocks;
    }


    /**
     * @return mixed
     */
    public function getFloorLayouts()
    {
        return $this->floorLayouts;
    }

    /**
     * @return mixed
     */
    public function getFz214()
    {
        return $this->fz_214;
    }

    /**
     * @param mixed $fz_214
     */
    public function setFz214($fz_214)
    {
        $this->fz_214 = $fz_214;
    }

    /**
     * @return mixed
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * @param mixed $dateStart
     */
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;
    }

    /**
     * @return mixed
     */
    public function getDeadlineYear()
    {
        return $this->deadlineYear;
    }

    /**
     * @param mixed $deadlineYear
     */
    public function setDeadlineYear($deadlineYear)
    {
        $this->deadlineYear = $deadlineYear;
    }

    /**
     * @return mixed
     */
    public function getDeadlineQuarter()
    {
        return $this->deadlineQuarter;
    }

    /**
     * @param mixed $deadlineQuarter
     */
    public function setDeadlineQuarter($deadlineQuarter)
    {
        $this->deadlineQuarter = $deadlineQuarter;
    }


    /**
     * @return mixed
     */
    public function getIsReady()
    {
        return $this->isReady;
    }

    /**
     * @param mixed $isReady
     */
    public function setIsReady($isReady)
    {
        $this->isReady = $isReady;
    }

    /**
     * @return mixed
     */
    public function getBlockCount()
    {
        return $this->blockCount;
    }

    /**
     * @param mixed $blockCount
     */
    public function setBlockCount($blockCount)
    {
        $this->blockCount = $blockCount;
    }

    /**
     * @return mixed
     */
    public function getFlatCount()
    {
        return $this->flatCount;
    }

    /**
     * @param mixed $flatCount
     */
    public function setFlatCount($flatCount)
    {
        $this->flatCount = $flatCount;
    }

    /**
     * @return mixed
     */
    public function getMaterialType()
    {
        return $this->materialType;
    }

    /**
     * @param mixed $materialType
     */
    public function setMaterialType($materialType)
    {
        $this->materialType = $materialType;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getFloorCount()
    {
        return $this->floorCount;
    }

    /**
     * @param mixed $floorCount
     */
    public function setFloorCount($floorCount)
    {
        $this->floorCount = $floorCount;
    }

    /**
     * @return array
     */
    public function getAggs()
    {
        return $this->aggs;
    }

    /**
     * @param array $aggs
     */
    public function setAggs(array $aggs)
    {
        $this->aggs = $aggs;
    }

    /**
     * @return mixed
     */
    public function getInstallment()
    {
        return $this->installment;
    }

    /**
     * @param mixed $installment
     */
    public function setInstallment($installment)
    {
        $this->installment = $installment;
    }

    /**
     * @return mixed
     */
    public function getMortgage()
    {
        return $this->mortgage;
    }

    /**
     * @param mixed $mortgage
     */
    public function setMortgage($mortgage)
    {
        $this->mortgage = $mortgage;
    }

    /**
     * @return mixed
     */
    public function getMilitaryMortgage()
    {
        return $this->militaryMortgage;
    }

    /**
     * @param mixed $militaryMortgage
     */
    public function setMilitaryMortgage($militaryMortgage)
    {
        $this->militaryMortgage = $militaryMortgage;
    }


}


