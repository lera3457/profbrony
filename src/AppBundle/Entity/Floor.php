<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Floor
 *
 * @ORM\Table(name="floor")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FloorRepository")
 */
class Floor
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="num", type="smallint")
     */
    private $num;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Block", inversedBy="floors")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $block;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Flat", mappedBy="floor")
     */
    private $flats;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\FloorLayout", inversedBy="floors")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $layout;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set num
     *
     * @param integer $num
     *
     * @return Floor
     */
    public function setNum($num)
    {
        $this->num = $num;

        return $this;
    }

    /**
     * Get num
     *
     * @return int
     */
    public function getNum()
    {
        return $this->num;
    }

    /**
     * @return mixed
     */
    public function getBlock()
    {
        return $this->block;
    }

    /**
     * @param mixed $block
     */
    public function setBlock($block)
    {
        $this->block = $block;
    }

    /**
     * @return mixed
     */
    public function getFlats()
    {
        return $this->flats;
    }

    /**
     * @return mixed
     */
    public function getLayout()
    {
        return $this->layout;
    }

    /**
     * @param mixed $layout
     */
    public function setLayout($layout)
    {
        $this->layout = $layout;
    }


}

