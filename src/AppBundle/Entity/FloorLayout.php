<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * FloorLayout
 *
 * @ORM\Table(name="floor_layout")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FloorLayoutRepository")
 */
class FloorLayout
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Floor", mappedBy="layout")
     */
    private $floors;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Block", inversedBy="floorLayout")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $block;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Area", mappedBy="layout")
     */
    private $areas;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Building", inversedBy="floorLayouts")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $building;

    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $image;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getAreas()
    {
        return $this->areas;
    }

    /**
     * @return mixed
     */
    public function getFloors()
    {
        return $this->floors;
    }

    /**
     * @return mixed
     */
    public function getBlock()
    {
        return $this->block;
    }

    /**
     * @param mixed $block
     */
    public function setBlock($block)
    {
        $this->block = $block;
    }

    /**
     * @return mixed
     */
    public function getBuilding()
    {
        return $this->building;
    }

    /**
     * @param mixed $building
     */
    public function setBuilding($building)
    {
        $this->building = $building;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

}

