<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Block
 *
 * @ORM\Table(name="block")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BlockRepository")
 */
class Block
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Building", inversedBy="blocks")
     * @ORM\JoinColumn(onDelete="CASCADE")*
     */
    private $building;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Floor", mappedBy="block")
     */
    private $floors;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\FloorLayout", mappedBy="block")
     */
    private $floorLayout;

    public function __toString()
    {
        return $this->getName() ? : '';
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getBuilding()
    {
        return $this->building;
    }

    /**
     * @param mixed $building
     */
    public function setBuilding($building)
    {
        $this->building = $building;
    }

    /**
     * @return mixed
     */
    public function getFloors()
    {
        return $this->floors;
    }

    /**
     * @return mixed
     */
    public function getFloorLayout()
    {
        return $this->floorLayout;
    }

    /**
     * @param mixed $floorLayout
     */
    public function setFloorLayout($floorLayout)
    {
        $this->floorLayout = $floorLayout;
    }

}

