<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * SubDistrictAdmin
 *
 * @ORM\Table(name="sub_district")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SubDistrictRepository")
 */
class SubDistrict
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\District", inversedBy="subDistricts")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $district;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Street", mappedBy="subDistrict")
     */
    private $streets;

    public function __construct()
    {
        $this->streets = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getName() ? : '';
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return SubDistrict
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * @param mixed $district
     */
    public function setDistrict($district)
    {
        $this->district = $district;
    }

    /**
     * @return mixed
     */
    public function getStreets()
    {
        return $this->streets;
    }

    /**
     * @param mixed $streets
     */
    public function setStreets($streets)
    {
        $this->streets = $streets;
    }

}

