<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Street
 *
 * @ORM\Table(name="street")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StreetRepository")
 */
class Street
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\District", inversedBy="streets")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $district;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\SubDistrict", inversedBy="streets")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $subDistrict;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Address", mappedBy="street")
     */
    private $address;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\StreetType", inversedBy="street")
     */
    private $type;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Street
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * @param mixed $district
     */
    public function setDistrict($district)
    {
        $this->district = $district;
    }

    /**
     * @return mixed
     */
    public function getSubDistrict()
    {
        return $this->subDistrict;
    }

    /**
     * @param mixed $subDistrict
     */
    public function setSubDistrict($subDistrict)
    {
        $this->subDistrict = $subDistrict;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

}

