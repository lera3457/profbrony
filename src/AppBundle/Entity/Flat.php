<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;

/**
 * Flat
 *
 * @ORM\Table(name="flat")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FlatRepository")
 */
class Flat
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(name="num", type="smallint")
     */
    private $num;

    /**
     * Комнатность( кол-во комнат)
     *
     * @ORM\Column(type="smallint")
     */
    private $roomCount;

    /**
     * Студия( да, нет)
     *
     * @ORM\Column(type="boolean")
     */
    private $studio;

    /**
     * В продаже от инвестора
     *
     * @ORM\Column(type="boolean")
     */
    private $investorSales;

    /**
     * Тип комнатности (студия(1к+студ),  евро-двушка(2к+студ), евро-трешка(3-х+студ))
     *
     * @ORM\Column(type="string", length=40)
     */
    private $roomType;

    /**
     * Цена за квартиру
     *
     * @ORM\Column(type="integer")
     */
    private $priceTotal;

    /**
     *Цена за кв.м -автоматически для каждой квартиры (Цена за квартиру/Площадь общая) - price_meter (price_total / area_total)
     *
     * @ORM\Column(type="integer")
     */
    private $priceMeter;

    /**
     * Цена в ипотеку
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $priceMortgage;

    /**
     * Цена в рассрочку
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $priceInstallment;

    /**
     * Площадь общая
     *
     * @ORM\Column(type="float")
     */
    private $areaTotal;

    /**
     * Площадь жилая
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $areaLiving;

    /**
     * Площадь кухни
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $kitchenArea;

    /**
     * Кол-во санузлов
     *
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $bathroomCount;

    /**
     * Площадь санузлов
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $bathroomArea;

    /**
     * Кол-во балконов
     *
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $balconyCount;

    /**
     * Площадь балкона
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $balconyArea;

    /**
     * Кол-во лоджий
     *
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $loggiaCount;

    /**
     * Площадь лоджии
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $loggiaArea;

    /**
     * Акции на квартиру ( да, нет)
     *
     * @ORM\Column(type="boolean")
     */
    private $action;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Floor", inversedBy="flats")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @Serializer\Exclude()
     */
    private $floor;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Area", inversedBy="flats")
     * @ORM\JoinColumn(onDelete="SET NULL")
     * @Serializer\Exclude()
     */
    private $area;

    /**
     * @ORM\Column(type="FlatStatusType", nullable=false)
     * @DoctrineAssert\Enum(entity="AppBundle\DBAL\Types\FlatStatusType")
     */
    private $status;

    /**
     * @ORM\Column(type="FlatDecorType", nullable=false)
     * @DoctrineAssert\Enum(entity="AppBundle\DBAL\Types\FlatDecorType")
     */
    private $decor;

    /**
     * Формируются в сеттерах, используются для сериализации
     */
    private $floorNum;
    private $blockName;



    /** Формирование цен за м2 и за всю квартиру, в зависимости от $isTotal
     *
     * @param int $price Цена
     * @param bool $isTotal true - общая цена; false - за м2
     */
    public function setPrice($price, $isTotal = true )
    {
        $areaTotal = $this->getAreaTotal();

        if ($price && $areaTotal) {

            if ($isTotal) {

                $this->setPriceTotal( $price );
                $this->setPriceMeter( $price / $areaTotal );

            } else {

                $this->setPriceMeter( $price );
                $this->setPriceTotal( $price * $areaTotal );

            }
        }
    }


    /**
     *  Фомирования типа комнатности
     *
     * @param mixed $roomType
     */
    public function setRoomType($roomType = '')
    {
        $roomCount = $this->roomCount;
        $studio = $this->studio;
        if ($roomCount == 1 && $studio == true) {
            $this->roomType = 'studio';
        } elseif ($roomCount == 2 && $studio == true) {
            $this->roomType = 'euro2';
        } elseif ($roomCount == 3 && $studio == true) {
            $this->roomType = 'euro3';
        } else {
            $this->roomType = $roomType;
        }
    }



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set num
     *
     * @param integer $num
     *
     * @return Flat
     */
    public function setNum($num)
    {
        $this->num = $num;

        return $this;
    }

    /**
     * Get num
     *
     * @return int
     */
    public function getNum()
    {
        return $this->num;
    }

    /**
     * @return mixed
     */
    public function getFloor()
    {
        return $this->floor;
    }

    /**
     * @param mixed $floor
     */
    public function setFloor($floor)
    {
        $this->floor = $floor;
    }

    /**
     * @return mixed
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * @param mixed $area
     */
    public function setArea($area)
    {
        $this->area = $area;
    }

    /**
     * @return mixed
     */
    public function getRoomCount()
    {
        return $this->roomCount;
    }

    /**
     * @param mixed $roomCount
     */
    public function setRoomCount($roomCount)
    {
        $this->roomCount = $roomCount;
    }

    /**
     * @return mixed
     */
    public function getStudio()
    {
        return $this->studio;
    }

    /**
     * @param mixed $studio
     */
    public function setStudio($studio)
    {
        $this->studio = $studio;
    }

    /**
     * @return mixed
     */
    public function getRoomType()
    {
        return $this->roomType;
    }


    /**
     * @return mixed
     */
    public function getPriceTotal()
    {
        return $this->priceTotal;
    }

    /**
     * @param mixed $priceTotal
     */
    public function setPriceTotal($priceTotal = 0)
    {
        $this->priceTotal = (int)$priceTotal;
    }

    /**
     * @return mixed
     */
    public function getPriceMeter()
    {
        return $this->priceMeter;
    }

    /**
     * @param mixed $priceMeter
     */
    public function setPriceMeter($priceMeter = 0)
    {
        $this->priceMeter = (int)$priceMeter;
    }


    /**
     * @return mixed
     */
    public function getAreaTotal()
    {
        return $this->areaTotal;
    }

    /**
     * @param mixed $areaTotal
     */
    public function setAreaTotal($areaTotal)
    {
        $this->areaTotal = $areaTotal;
    }

    /**
     * @return mixed
     */
    public function getAreaLiving()
    {
        return $this->areaLiving;
    }

    /**
     * @param mixed $areaLiving
     */
    public function setAreaLiving($areaLiving)
    {
        $this->areaLiving = $areaLiving;
    }

    /**
     * @return mixed
     */
    public function getKitchenArea()
    {
        return $this->kitchenArea;
    }

    /**
     * @param mixed $kitchenArea
     */
    public function setKitchenArea($kitchenArea)
    {
        $this->kitchenArea = $kitchenArea;
    }

    /**
     * @return mixed
     */
    public function getBathroomCount()
    {
        return $this->bathroomCount;
    }

    /**
     * @param mixed $bathroomCount
     */
    public function setBathroomCount($bathroomCount)
    {
        $this->bathroomCount = $bathroomCount;
    }

    /**
     * @return mixed
     */
    public function getBathroomArea()
    {
        return $this->bathroomArea;
    }

    /**
     * @param mixed $bathroomArea
     */
    public function setBathroomArea($bathroomArea)
    {
        $this->bathroomArea = $bathroomArea;
    }

    /**
     * @return mixed
     */
    public function getBalconyCount()
    {
        return $this->balconyCount;
    }

    /**
     * @param mixed $balconyCount
     */
    public function setBalconyCount($balconyCount)
    {
        $this->balconyCount = $balconyCount;
    }

    /**
     * @return mixed
     */
    public function getBalconyArea()
    {
        return $this->balconyArea;
    }

    /**
     * @param mixed $balconyArea
     */
    public function setBalconyArea($balconyArea)
    {
        $this->balconyArea = $balconyArea;
    }

    /**
     * @return mixed
     */
    public function getLoggiaCount()
    {
        return $this->loggiaCount;
    }

    /**
     * @param mixed $loggiaCount
     */
    public function setLoggiaCount($loggiaCount)
    {
        $this->loggiaCount = $loggiaCount;
    }

    /**
     * @return mixed
     */
    public function getLoggiaArea()
    {
        return $this->loggiaArea;
    }

    /**
     * @param mixed $loggiaArea
     */
    public function setLoggiaArea($loggiaArea)
    {
        $this->loggiaArea = $loggiaArea;
    }

    /**
     * @return mixed
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param mixed $action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }

    /**
     * @return mixed
     */
    public function getFloorNum()
    {
        return $this->floorNum;
    }

    /**
     * @param mixed $floorNum
     */
    public function setFloorNum($floorNum)
    {
        $this->floorNum = $floorNum;
    }

    /**
     * @return mixed
     */
    public function getBlockName()
    {
        return $this->blockName;
    }

    /**
     * @param mixed $blockName
     */
    public function setBlockName($blockName)
    {
        $this->blockName = $blockName;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getDecor()
    {
        return $this->decor;
    }

    /**
     * @param mixed $decor
     */
    public function setDecor($decor)
    {
        $this->decor = $decor;
    }

    /**
     * @return mixed
     */
    public function getPriceMortgage()
    {
        return $this->priceMortgage;
    }

    /**
     * @param mixed $priceMortgage
     */
    public function setPriceMortgage($priceMortgage)
    {
        $this->priceMortgage = $priceMortgage;
    }

    /**
     * @return mixed
     */
    public function getPriceInstallment()
    {
        return $this->priceInstallment;
    }

    /**
     * @param mixed $priceInstallment
     */
    public function setPriceInstallment($priceInstallment)
    {
        $this->priceInstallment = $priceInstallment;
    }

    /**
     * @return mixed
     */
    public function getInvestorSales()
    {
        return $this->investorSales;
    }

    /**
     * @param mixed $investorSales
     */
    public function setInvestorSales($investorSales)
    {
        $this->investorSales = $investorSales;
    }
}

