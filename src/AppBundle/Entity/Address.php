<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Address
 *
 * @ORM\Table(name="address")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AddressRepository")
 */
class Address
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string;
     * @ORM\Column(name="street_num", type="string", length=10)
     */
    private $streetNum;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Street", inversedBy="address")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $street;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Developer", mappedBy="address")
     */
    private $developer;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Agency", mappedBy="address")
     */
    private $agency;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Object", mappedBy="address")
     */
    private $object;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Building", mappedBy="address")
     */
    private $building;

    public function __toString()
    {
        return $this->getName() ? : '';
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Address
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param mixed $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * @return mixed
     */
    public function getDeveloper()
    {
        return $this->developer;
    }

    /**
     * @return mixed
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * @return mixed
     */
    public function getBuilding()
    {
        return $this->building;
    }

    /**
     * @return mixed
     */
    public function getAgency()
    {
        return $this->agency;
    }

    /**
     * @param mixed $agency
     */
    public function setAgency($agency)
    {
        $this->agency = $agency;
    }

    /**
     * @return string
     */
    public function getStreetNum()
    {
        return $this->streetNum;
    }

    /**
     * @param string $streetNum
     */
    public function setStreetNum($streetNum)
    {
        $this->streetNum = $streetNum;
    }

}

