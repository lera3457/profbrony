<?php

namespace AppBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class QuarterType extends AbstractEnumType
{
    const Q1 = 'quarter1';
    const Q2 = 'quarter2';
    const Q3 = 'quarter3';
    const Q4 = 'quarter4';

    protected static $choices = [
        self::Q1 => 'I-й кв.',
        self::Q2 => 'II-й кв.',
        self::Q3 => 'III-й кв.',
        self::Q4 => 'IV-й кв.',
    ];
}