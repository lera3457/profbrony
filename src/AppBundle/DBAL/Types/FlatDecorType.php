<?php

namespace AppBundle\DBAL\Types;

class FlatDecorType extends AbstractEnumTypeExtended
{
    const WITHOUT_DECOR = 'without_decor';
    const ROUGH = 'rough';
    const FINISH = 'finish';
    const FULL = 'full';

    protected static $choices = [
        self::WITHOUT_DECOR => 'Без отделки',
        self::ROUGH => 'Черновой',
        self::FINISH => 'Чистовой',
        self::FULL => 'Под ключ',
    ];
}