<?php

namespace AppBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class BuildingMaterialType extends AbstractEnumType
{
    const BRICK = 'brick';
    const MONOLITH = 'monolith';
    const PANEL = 'panel';

    protected static $choices = [
        self::BRICK => 'Кирпич',
        self::MONOLITH => 'Монолит',
        self::PANEL => 'Панель',
    ];
}