<?php

namespace AppBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class BuildingType extends AbstractEnumType
{
    const COMFORT = 'comfort';
    const COMFORT_PLUS = 'comfort_plus';
    const BUSINESS = 'business';
    const ECONOMY = 'economy';
    const ELITE = 'elite';

    protected static $choices = [
        self::COMFORT => 'Комфорт',
        self::COMFORT_PLUS => 'Комфорт+',
        self::BUSINESS => 'Бизнес',
        self::ECONOMY => 'Эконом',
        self::ELITE => 'Элит',
    ];
}