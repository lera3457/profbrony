<?php

namespace AppBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;
/**
 * Тип комнатности (студия(1к+студ),  евро-двушка(2к+студ), евро-трешка(3-х+студ))
 * */
class RoomType extends AbstractEnumType
{
    const STUDIO = 'studio';
    const EURO2 = 'euro2';
    const EURO3 = 'euro3';
    const DEFAULT = '';

    protected static $choices = [
        self::STUDIO => 'Студия',
        self::EURO2 => 'Евро-двушка',
        self::EURO3 => 'Евро-трешка',
        self::DEFAULT => '',
    ];
}