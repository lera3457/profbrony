<?php

namespace AppBundle\DBAL\Types;

class FlatStatusType extends AbstractEnumTypeExtended
{
    const FREE = 'free';
    const SOLD_OUT = 'sold_out';
    const BOOKED = 'booked';
    const RESERVED = 'reserved';

    protected static $choices = [
        self::FREE => 'Свободно',
        self::SOLD_OUT => 'Продано',
        self::BOOKED => 'Забронировано',
        self::RESERVED => 'В резерве',
    ];
}