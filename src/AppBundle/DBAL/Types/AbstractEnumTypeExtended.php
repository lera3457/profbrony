<?php

namespace AppBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

/**
 * Class AbstractEnumTypeExtended
 * @package AppBundle\DBAL\Types
 */
abstract class AbstractEnumTypeExtended extends AbstractEnumType
{

    /**
     * Поиск ключа enum по значению
     * @param string $value
     * @static
     * @return false|int|string
     */
    public static function getEnumByValue(string $value)
    {
        $values = self::getReadableValues();
        $enum = array_search($value, $values);
        $keys = array_keys($values);
        // Если не найден, то по умолчанию первый элемент
        $enum = $enum ? : $keys[0];

        return $enum;
    }
}
