<?php

namespace AppBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class ObjectType extends AbstractEnumType
{
    const COMPLEX = 'complex';
    const DISTRICT = 'district';

    protected static $choices = [
        self::COMPLEX => 'Комплекс',
        self::DISTRICT => 'Район',
    ];
}