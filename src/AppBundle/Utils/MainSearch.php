<?php
namespace AppBundle\Utils;

use AppBundle\DBAL\Types\BuildingMaterialType;
use AppBundle\DBAL\Types\BuildingType;
use AppBundle\DBAL\Types\FlatDecorType;
use AppBundle\DBAL\Types\FlatStatusType;
use AppBundle\Entity\District;
use AppBundle\Entity\SubDistrict;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\ResultSetMapping;
use Elastica\Aggregation\Filters;
use Elastica\Aggregation\Max;
use Elastica\Aggregation\Min;
use Elastica\Aggregation\Stats;
use Elastica\Client;
use Elastica\Query;
use Elastica\Query\BoolQuery;
use Elastica\Query\Match;
use Elastica\Query\Nested;
use Elastica\Query\Range;
use Elastica\Query\Term;
use Elastica\Query\Terms;
use Elastica\QueryBuilder\DSL\Filter;
use Elastica\Test\Aggregation\NestedTest;
use Symfony\Component\HttpFoundation\Request;
use FOS\ElasticaBundle\Finder\TransformedFinder;

use AppBundle\DBAL\Types\QuarterType;


/**
 * Class MainSearch
 * @package AppBundle\Utils
 */
class MainSearch
{

    /**
     *  Поиск
     *
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return array
     */
    public function searchFilter(Request $request, EntityManagerInterface $em)
    {

        $query = $request->query;

        $priceType = $query->has('price') && $query->get('price') == 'meter' ? 'priceMeter' : 'priceTotal';

        // формирование запроса
        $qb = $em->createQueryBuilder();
        $qb
            ->select('building, block, floor, flat')
            ->from('AppBundle:Building', 'building')
            ->join('building.object', 'object')
            ->join('object.developer', 'developer')
            ->join('building.address', 'address')
            ->join('address.street', 'street')
            ->join('street.district', 'district')
            ->leftJoin('street.subDistrict', 'subDistrict')
            ->leftJoin('building.blocks', 'block')
            ->leftJoin('block.floors', 'floor')
            ->leftJoin('floor.flats', 'flat')
        ;


        // Фильтрация по умолчанию
        // Если квартира продана, не учитываем
      //  $qb->andWhere( "flat.status NOT LIKE '%sold_out%'" );


        // --- ОСНОВНОЙ ФИЛЬТР --- //


        // Районы и под районы
        if ( $query->has('district') || $query->has('subDistrict') ) {

            $districts = $query->has('district') ? explode(',', $query->get('district')) : array();
            $subDistricts = $query->has('subDistrict') ? explode(',', $query->get('subDistrict')) : null;
            $sql = '';
            $params = array();

            if (is_array($subDistricts)) {

                // подготовка подрайонов
                $subDistricts = array_map(function($v) { return explode('|', $v); }, $subDistricts);

                foreach ($subDistricts as $key => $subDistrict) {

                    // два элемента - 1) район; 2) подрайон
                    if ( is_array($subDistrict) && count($subDistrict) == 2 ) {

                        // если есть район в массиве с подрайонами, то удаляем из районов для детализации поиска,
                        // т.е. ищутся только те дома, которые есть в подрайонах.
                        if ( false !== $districtForUnset = array_search($subDistrict[0], $districts) ) {
                            unset( $districts[$districtForUnset] );
                        }

                        $sql .= "(district.id = :district_$key AND subDistrict.id = :subDistrict_$key) OR";
                        $params["district_$key"] = $subDistrict[0];
                        $params["subDistrict_$key"] = $subDistrict[1];

                    }
                }
                $sql = substr($sql, 0, -3);
            }

            // если районы остаются после перебора подрайонов, добавляем в фильтр
            if (is_array($districts) && !empty($districts)) {

                $sql = empty($sql) ? "district.id IN (:districts)" : "($sql) OR (district.id IN (:districts))";
                $params['districts'] = $districts;
            }

            $qb->andWhere($sql);
            $qb->setParameters($params);

            unset($districts, $districts, $districtForUnset, $subDistricts, $subDistrict, $sql, $params);
        }


        // Срок сдачи
        if ($query->has('deadline')) {

            $sql = '';
            $params = array();
            $deadlines = explode(',', $query->get('deadline'));

            if (is_array($deadlines)) {

                $deadlines = array_map(function($e) { return explode('|', $e); }, $deadlines);

                foreach ($deadlines as $key => $deadline) {
                    if ( is_array($deadline) && count($deadline) == 2 ) {
                        $date = date('Y-m-d', mktime(0, 0, 0, 1, 1, $deadline[1]));
                        $sql .= "(building.deadlineYear = :year_$key AND building.deadlineQuarter = :quarter_$key ) OR";
                        $params["year_$key"] = $date;
                        $params["quarter_$key"] = $deadline[0];
                    }
                }
                if (!empty($params) && $sql !== '') {
                    $sql = substr($sql, 0, -3);
                    $qb->andWhere($sql);
                    $qb->setParameters($params);
                }
            }

            unset($deadlines, $deadline, $params, $sql, $key);
        }


        // Квартирность + Студия
        if ($query->has('rC')) {

            $roomCount = explode(',', $query->get('rC'));
            $sql = '';

            if (is_array($roomCount)) {

                // все студии
                if (in_array('sa', $roomCount)) {

                    $sql .= '( flat.studio = 1 ) OR ';

                    unset( $roomCount[array_search('sa', $roomCount)] );

                    $roomCount = !empty($roomCount) ? $roomCount : array(1, 2, 3, 4);
                }

                // 1к студии
                if (in_array('s1', $roomCount)) {

                    $sql .= '( flat.studio = 1 AND flat.roomCount = 1 ) OR';

                    unset( $roomCount[array_search('s1', $roomCount)] );

                    $roomCount = !empty($roomCount) ? $roomCount : array(1, 2, 3, 4);
                }


                if (!empty($roomCount)) {
                    $roomCount = implode(",", $roomCount);
                    $sql .= " flat.roomCount IN ($roomCount) OR";
                }

                $sql = $sql ? '(' . substr($sql, 0, -3) . ')' : '';

                if ($sql) {
                    $qb->andWhere($sql);
                }

            }


            unset($roomCount);
        }


        // Фильтрация по минимальной цене
        if ($query->has('priceMin')) {
            $qb->andWhere( $qb->expr()->gte("flat.$priceType", (int)$query->get('priceMin')) );
        }


        // Фильтрация по максимальной цене
        if ($query->has('priceMax')) {
            $qb->andWhere( $qb->expr()->lte("flat.$priceType", (int)$query->get('priceMax')) );
        }


        // Акция
        if ($query->has('action')) {
            $qb->andWhere( $qb->expr()->eq('flat.action', true) );
        }

        // Строка поиска
        if ($query->has('q')) {
            $qb->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->like(
                        $qb->expr()->lower('object.name'), "'%" . $query->get('q') . "%'"
                    ),
                    $qb->expr()->like(
                        $qb->expr()->lower('developer.name'), "'%" . $query->get('q') . "%'"
                    )
                )
            );
        }

        // Литера
        if ($query->has('litera')) {
            $qb->andWhere(
                $qb->expr()->like(
                    $qb->expr()->lower('building.name'),
                    "'%" . $query->get('litera') . "%'"
                )
            );
        }


        // --- ДОПОЛНИТЕЛЬНЫЙ ФИЛЬТР --- //

        // площадь м2 от
        if ($query->has('armf')) {
            $qb->andWhere( $qb->expr()->gte('flat.areaTotal', (int)$query->get('armf')) );
        }

        // площадь м2 до
        if ($query->has('armt')) {
            $qb->andWhere( $qb->expr()->lte('flat.areaTotal', (int)$query->get('armt')) );
        }

        // площадь кухни до
        if ($query->has('ark')) {
            $qb->andWhere( $qb->expr()->lte('flat.kitchenArea', (int)$query->get('ark')) );
        }


        if ($query->has('floorFrom')) {
            $qb->andWhere( $qb->expr()->gte('floor.num', (int)$query->get('floorFrom')) );
        }

        if ($query->has('floorTo')) {
            $qb->andWhere( $qb->expr()->lte('floor.num', (int)$query->get('floorTo')) );
        }

        if ($query->has('noFirstFloor')) {
            $qb->andWhere( $qb->expr()->neq('floor.num', 1) );
        }

        if ($query->has('noLastFloor')) {
            $qb->andWhere( $qb->expr()->neq('floor.num', 'building.floorCount') );
        }

        if ($query->has('ready')) {

            if ($query->get('ready') == 1) {
                $qb->andWhere( $qb->expr()->eq('building.isReady', 1 ) );
            } else {
                $qb->andWhere( $qb->expr()->eq('building.isReady', 0 ) );
            }

        }

        if ($query->has('decor')) {
            $decor = explode(',', $query->get('decor'));
            if (is_array($decor)) {
                $qb->andWhere( $qb->expr()->in('flat.decor', $decor) );
            }
        }

        if ($query->has('material')) {
            $material = explode(',', $query->get('material'));
            if (is_array($material)) {
                $qb->andWhere( $qb->expr()->in('flat.decor', $material) );
            }
        }

        if ($query->has('type')) {
            $type = explode(',', $query->get('type'));
            if (is_array($type)) {
                $qb->andWhere( $qb->expr()->in('building.type', $type) );
            }
        }

        if ($query->has('installment')) {
            $qb->andWhere( $qb->expr()->eq('building.installment', true ) );
        }

        if ($query->has('mortgage')) {
            $qb->andWhere( $qb->expr()->eq('building.mortgage', true ) );
        }

        if ($query->has('militaryMortgage')) {
            $qb->andWhere( $qb->expr()->eq('building.militaryMortgage', true ) );
        }

        if ($query->has('fz')) {
            $qb->andWhere( $qb->expr()->eq('building.fz_214', true ) );
        }

        if ($query->has('loggia')) {
            $qb->andWhere( $qb->expr()->gt('flat.loggiaCount', 0 ) );
        }

        if ($query->has('balcony')) {
            $qb->andWhere( $qb->expr()->gt('flat.balconyCount', 0 ) );
        }




        // Выборка данных
        $q = $qb->getQuery();
        $result = $q->getResult();

        // Добавление агрегаций к результату
        if ($result) {

            foreach ($result as $building) {

                $priceArray = array();
                $areaArray = array();
                $aggs = array(
                    'roomCount' => array(1 => 0, 2 => 0, 3 => 0, 4 => 0),
                    'flatCount' => 0,
                    'priceMin' => 0,
                    'areaMeterMin' => 0,
                    'areaMeterMax' => 0,
                );

                foreach ($building->getBlocks() as $block) {
                    foreach ($block->getFloors() as $floor) {
                        foreach ($floor->getFlats() as $flat) {

                            // Если есть параметр "студия", то выводить данные на одну комнатность меньше.
                            // Например, если евро-трешка, то выводим информацию как за 2-х комнатную квартиру.
                            // Если студия, то однокомнатная.
                            $roomCount = $flat->getRoomCount();
                            $isStudio = $flat->getStudio();
                            $roomCount = ($isStudio && $roomCount > 1) ? $roomCount - 1 : $roomCount;
                            $aggs['roomCount'][$roomCount]++;

                            if ($flat->getPriceMeter() > 0) {
                                $priceArray[] = $flat->getPriceMeter();
                            }

                            if ($flat->getAreaTotal() > 0) {
                                $areaArray[] = $flat->getAreaTotal();
                            }
                        }

                        $aggs['flatCount'] += $floor->getFlats()->count();
                    }
                }

                if (!empty($priceArray)) {
                    $aggs['priceMin'] = min($priceArray);
                }

                if (!empty($areaArray)) {
                    $aggs['areaMeterMin'] = min($areaArray);
                    $aggs['areaMeterMax'] = max($areaArray);
                }

                $building->setAggs($aggs);
            }
        }

        return $result;
    }



    /**
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return array
     */

    public function breadcrumbs( Request $request, EntityManagerInterface $em )
    {
        $query = $request->query;

        $crumb = array();
        $breadcrumbs = array();


        // Районы и под районы
        if ( $query->has('district') || $query->has('subDistrict') ) {
            $districts = $query->has('district') ? explode(',', $query->get('district')) : array();
            $subDistricts = $query->has('subDistrict') ? explode(',', $query->get('subDistrict')) : array();

            if (is_array($subDistricts)) {

                // подготовка подрайонов
                $subDistricts = array_map(function($v) { return explode('|', $v); }, $subDistricts);

                foreach ($subDistricts as $subDistrict) {

                    if (!in_array($subDistrict[0], $districts)) {
                        $districts[] = $subDistrict[0];
                    }

                    $subDistrict = $em->getRepository(SubDistrict::class)->find($subDistrict[1]);

                    if ($subDistrict) {
                        $crumb['subDistrict'][] = $subDistrict->getName();
                    }


                }
            }

            // если районы остаются после перебора подрайонов, добавляем в фильтр
            if (is_array($districts) && !empty($districts)) {

                foreach ($districts as $district) {
                    $district = $em->getRepository(District::class)->find($district);

                    if ($district) {
                        $crumb['district'][] = $district->getName();
                    }

                }

            }

            if (isset($crumb['district'])) {
                $breadcrumbs[] = implode(',', $crumb['district']);
            }

            if (isset($crumb['subDistrict'])) {
                $breadcrumbs[] = implode(',', $crumb['subDistrict']);
            }

        }


        // Срок сдачи
        if ($query->has('deadline')) {

            $deadlines = explode(',', $query->get('deadline'));

            if (is_array($deadlines)) {

                $deadlines = array_map(function($e) { return explode('|', $e); }, $deadlines);
                $deadlineMap = array(
                    'quarter1' => 'I-й кв.',
                    'quarter2' => 'II-й кв.',
                    'quarter3' => 'III-й кв.',
                    'quarter4' => 'IV-й кв.',
                );

                foreach ($deadlines as $deadline) {
                    if ( is_array($deadline) && count($deadline) == 2 ) {
                        $crumb['deadline'][] = $deadlineMap[$deadline[0]] . ' '. $deadline[1];
                    }
                }

                $breadcrumbs[] = implode(',', $crumb['deadline']);
            }

            unset($deadlines, $deadline);

        }


        // Квартирность + Студия
        if ($query->has('rC')) {
            $roomCount = explode(',', $query->get('rC'));

            $roomCountMap = array(
                'sa' => 'Все студии', 's1' => '1к студия', 1 => '1к', 2 => '2к', 3 => '3к', 4 => '4к'
            );

            foreach ($roomCount as $item) {
                $crumb['rC'][] = $roomCountMap[$item];
            }

            $breadcrumbs[] = implode(',', $crumb['rC']);

        }


        // Фильтрация по минимальной цене
        if ($query->has('priceMin') || $query->has('priceMax')) {
            $priceType = $query->has('price') && $query->get('price') == 'meter' ? ' за м2' : ' за все';
            if ($query->has('priceMin') && $query->has('priceMax')) {
                $breadcrumbs[] = 'от ' . $query->get('priceMin') . ' до '
                    . $query->get('priceMax') . 'р.' . $priceType;
            } else {
                if ($query->has('priceMin')) {
                    $breadcrumbs[] = 'от ' . $query->get('priceMin') . 'р.' . $priceType;
                }
                if ($query->has('priceMax')) {
                    $breadcrumbs[] = 'до ' . $query->get('priceMax') . 'р.' . $priceType;
                }
            }

        }



        // Акция
        if ($query->has('action')) $breadcrumbs[] = 'Акция';

        // Строка поиска
        if ($query->has('q')) $breadcrumbs[] = '"'.$query->get('q').'"';

        // Литера
        if ($query->has('litera')) $breadcrumbs[] = '"'.$query->get('litera').'"';

        // Площадь м2
        if ($query->has('armf') || $query->has('armt')) {

            if ($query->has('armf') && $query->has('armt')) {
                $breadcrumbs[] = 'от ' . $query->get('armf') . ' до ' . $query->get('armt') . ' м2';
            } else {
                if ($query->has('armf')) {
                    $breadcrumbs[] = 'от ' . $query->get('armf') . ' м2';
                }
                if ($query->has('armt')) {
                    $breadcrumbs[] = 'до ' . $query->get('armt') . ' м2';
                }
            }

        }


        // Площадь кухни до
        if ($query->has('ark')) $breadcrumbs[] = 'кухня до'.$query->get('ark').' м2';

        // Этажность
        if ($query->has('floorFrom') || $query->has('floorTo')) {

            if ($query->has('floorFrom') && $query->has('floorTo')) {
                $breadcrumbs[] = 'Этажи от ' . $query->get('floorFrom') . ' до ' . $query->get('floorTo');
            } else {
                if ($query->has('floorFrom')) {
                    $breadcrumbs[] = 'Этажи от ' . $query->get('floorFrom');
                }
                if ($query->has('floorTo')) {
                    $breadcrumbs[] = 'Этажи до ' . $query->get('floorTo');
                }
            }

        }


        if ($query->has('noFirstFloor')) $breadcrumbs[] = 'Без первого этажа';

        if ($query->has('noLastFloor')) $breadcrumbs[] = 'Без последнего этажа';

        if ($query->has('ready')) {
            $breadcrumbs[] = $query->get('ready') == 1 ? 'Сдан' : 'Не сдан';
        }

        if ($query->has('decor')) {
            $decor = explode(',', $query->get('decor'));
            if (is_array($decor)) {
                $str = '';
                foreach ($decor as $item) {
                    $str .= FlatDecorType::getReadableValue($item) . ', ';
                }
                $breadcrumbs[] = substr($str, 0, -2);
            }
        }

        if ($query->has('material')) {
            $material = explode(',', $query->get('material'));
            if (is_array($material)) {
                $str = '';
                foreach ($material as $item) {
                    $str .= BuildingMaterialType::getReadableValue($item) . ', ';
                }
                $breadcrumbs[] = substr($str, 0, -2);
            }
        }

        if ($query->has('type')) {
            $type = explode(',', $query->get('type'));
            if (is_array($type)) {
                $str = '';
                foreach ($type as $item) {
                    $str .= BuildingType::getReadableValue($item) . ', ';
                }
                $breadcrumbs[] = substr($str, 0, -2);
            }
        }

        if ($query->has('installment')) $breadcrumbs[] = 'Рассрочка';

        if ($query->has('mortgage')) $breadcrumbs[] = 'Ипотека';

        if ($query->has('militaryMortgage')) $breadcrumbs[] = 'Военная ипотека';

        if ($query->has('fz')) $breadcrumbs[] = 'ФЗ-214';

        if ($query->has('loggia')) $breadcrumbs[] = 'Лоджия';

        if ($query->has('balcony')) $breadcrumbs[] = 'Балкон';



        return $breadcrumbs;
    }


// :TODO Настроить elasticsearch.)

//    public function searchFilter(Request $request, TransformedFinder $finder )
//    {
//
//        $query = $request->query;
//        $boolQuery = new BoolQuery();
//        $priceType = $query->has('price') && $query->get('price') == 'meter' ? 'priceMeter' : 'priceTotal';
//        $roomCount = $query->has('roomCount') ? explode(',', $query->get('roomCount')) : array(1, 2, 3, 4);
//        $priceMin = $query->has('priceMin') ? $query->get('priceMin') : 1;
//        $priceMax = $query->has('priceMax') ? $query->get('priceMax') : 999999999;
//
//        // Районы
//        if ( $query->has('district')) {
//            $districtQuery = new Terms();
//            $districtQuery->setTerms('address.street.district.id', explode(',', $query->get('district')));
//            $boolQuery->addMust($districtQuery);
//        }
//
//        // Под районы
//        if ( $query->has('subDistrict')) {
//            $subDistrictQuery = new Terms();
//            $subDistrictQuery->setTerms('address.street.subDistrict.id', explode(',', $query->get('subDistrict')));
//            $boolQuery->addMust($subDistrictQuery);
//        }
//
//        if ($query->has('deadline')) {
//
//            $deadline = explode(',', $query->get('deadline'));
//
//            if (is_array($deadline)) {
//                $deadline = array_map(function($e) {
//                    return explode('_', $e);
//                }, $deadline);
//
//                foreach ($deadline as $k => $v) {
//                    ${'quarter'.($k+1)} = isset($v[0]) ? $v[0] : null;
//                    $deadlineYear = isset($v[1]) ? $v[1] : null;
//
//                    if ($deadlineQuarter && $deadlineYear) {
//                        $deadlineYearQuery = new Term();
//                        $deadlineYearQuery->setTerm('deadlineYear', $deadlineYear);
//                        $deadlineQuarterQuery = new Term();
//                        $deadlineQuarterQuery->setTerm('deadlineQuarter', $deadlineQuarter);
//                        $boolQuery->addMust($deadlineYearQuery);
//                        $boolQuery->addMust($deadlineQuarterQuery);
//                    }
//                }
//
//            }
//        }
//
//
//        if ($query->has('roomCount')) {
//
//            if (in_array('studio', $roomCount)) {
//                $studioQuery = new Term(array('blocks.floors.flats.studio' => true));
//                $studioNested = new Nested();
//                $studioNested->setPath('blocks.floors.flats');
//                $studioNested->setQuery($studioQuery);
//                $boolQuery->addMust($studioNested);
//                unset( $roomCount[array_search('studio', $roomCount)] );
//                $roomCount = !empty($roomCount) ? $roomCount : array(1, 2, 3, 4);
//            }
//
//            if (!empty($roomCount)) {
//
//                $roomCountQuery = new Terms();
//                $roomCountQuery->setTerms('blocks.floors.flats.roomCount', $roomCount);
//                $roomCountNested = new Nested();
//                $roomCountNested->setPath('blocks.floors.flats');
//                $roomCountNested->setQuery($roomCountQuery);
//                $boolQuery->addMust($roomCountNested);
//            }
//        }
//
//        if ($query->has('name')) {
//            $nameQuery = new Match('name', $query->get('name'));
//            $boolQuery->addShould($nameQuery);
//        }
//
//        $priceRange = new Range('blocks.floors.flats.' . $priceType,
//            array('gt' => 0,'gte' => $priceMin, 'lte' => $priceMax,
//            ));
//        $priceNested = new Nested();
//        $priceNested->setPath('blocks.floors.flats');
//        $priceNested->setQuery($priceRange);
//        $boolQuery->addFilter($priceNested);
//
//        /**
//         * Фильтрация по умолчанию
//         */
//
//        // Если общая площад равна 0, не учитываем
//        //$nestedDefaultArea = new Nested();
//        //$nestedDefaultArea->setPath('blocks.floors.flats');
//        //$nestedDefaultArea->setQuery(
//        //    new Range('blocks.floors.flats.areaTotal', array('gt' => 0)));
//        //$boolQuery->addFilter($nestedDefaultArea);
//
//        // Если квартира продана, не учитываем
//        $nestedDefaultStatus = new Nested();
//        $nestedDefaultStatus->setPath('blocks.floors.flats');
//        $nestedDefaultStatus->setQuery(
//            new Terms('blocks.floors.flats.status', array('free', 'booked', 'reserved'))
//        );
//
//        $boolQuery->addFilter($nestedDefaultStatus);
//
//
//
//
//
//        /**
//         * Настройка агрегаций и фильтраций
//         */
//
//        $aggRoomCount = new \Elastica\Aggregation\Terms('roomCount');
//        $aggRoomCount->setField('blocks.floors.flats.roomCount');
//        $aggRoomCount->setOrder('_term', 'asc');
//
//        $aggAreaMin = new Min('areaMin');
//        $aggAreaMin->setField('blocks.floors.flats.areaTotal');
//
//        $aggAreaMax = new Max('areaMax');
//        $aggAreaMax->setField('blocks.floors.flats.areaTotal');
//
//        $aggStatus = new \Elastica\Aggregation\Terms('status');
//        $aggStatus->setField('blocks.floors.flats.status');
//
//        $aggPriceTotal = new Min('priceTotal');
//        $aggPriceTotal->setField('blocks.floors.flats.priceTotal');
//
//        $aggFilterAreaTotal = new Filters('filterAreaTotal');
//        $aggFilterAreaTotal->addFilter(
//            new Range('blocks.floors.flats.areaTotal', array('gt' => 0)),
//            'areaTotal'
//        );
//        $aggFilterAreaTotal->addAggregation($aggAreaMin);
//        $aggFilterAreaTotal->addAggregation($aggAreaMax);
//
//        $aggFilterRoomCount = new \Elastica\Aggregation\Filter(
//            'filterRoomCount',
//            new Terms('blocks.floors.flats.roomCount', $roomCount
//            )
//        );
//
//        $aggFilterPriceTotal = new \Elastica\Aggregation\Filter('filterPriceTotal',
//            new Range('blocks.floors.flats.' . $priceType,
//                array('gt' => 0, 'gte' => $priceMin, 'lte' => $priceMax))
//        );
//
//        $aggFilterStatus = new \Elastica\Aggregation\Filter(
//            'filterStatus',
//            new Terms('blocks.floors.flats.status', array('free', 'booked', 'reserved')));
//
//
//        $aggFlats = new \Elastica\Aggregation\Nested('flats', 'blocks.floors.flats');
//        $aggFilterRoomCount->addAggregation($aggRoomCount);
//        $aggFilterPriceTotal->addAggregation($aggFilterRoomCount);
//        $aggFilterStatus->addAggregation($aggFilterPriceTotal);
//        $aggFlats->addAggregation($aggFilterStatus);
//
//
//        /**
//         * Подключение агрегаций к основному запросу
//         */
//        $agg = new \Elastica\Aggregation\Terms('buildings');
//        $agg->setField('id');
//        $agg->addAggregation($aggFlats);
//
//
//        $aggQuery = new Query();
//        $aggQuery->addAggregation($agg);
//        $aggQuery->setQuery($boolQuery);
//
//        // Получаем агрегацию по нужным параметрам
//        $client = new Client();
//        $index = $client->getIndex('app');
//        $aggs = $index->search($aggQuery)->getAggregation('buildings');
//
//
//
//
//
//        // Результата фильтра
//        $result = $finder-> find($boolQuery);
//
//        //Присоединение агрегаций к результату выборки.
//        if (isset($aggs['buckets']) && $result) {
//            foreach ($result as $k => $item) {
//                $aggKey = array_search($item->getId(), array_column($aggs['buckets'], 'key'));
//                if ($aggKey !== false) {
//                    $result[$k]->setAggs( $aggs['buckets'][$aggKey]);
//                } else {
//                    $result[$k]->setAggs( array());
//                }
//                unset($aggKey);
//            }
//        }
//
//
//
//
//
//
//
//
//        // Перебор результата для выборки статистики
//        /**
//        foreach ($result as $building) {
//            $roomCount = array(1 => 0, 2 => 0, 3 => 0, 4 => 0);
//            $blocks = $building->getBlocks();
//            foreach ($blocks as $block) {
//                $floors = $block->getFloors();
//                foreach ($floors as $floor) {
//                    $flats = $floor->getFlats();
//                    foreach ($flats as $flat) {
//                        $roomCount[$flat->getRoomCount()]++;
//                    }
//                }
//            }
//
//            $agg = array('roomCount' => $roomCount);
//
//            $building->setAggs($agg);
//        }
//         **/
//
//        return $result;
//    }


}

