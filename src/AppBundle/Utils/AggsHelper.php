<?php

namespace AppBundle\Utils;

use AppBundle\Entity\Building;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class AggsHelper
 * @package AppBundle\Utils
 */
class AggsHelper
{
    /**
     *  Пересчет свободных квартир в доме
     * @param $building
     * @return int
     */
    public function countFlatsInBuilding(Building $building)
    {
        $flatCount = 0;

        foreach ($building->getBlocks() as $block) {
            foreach ($block->getFloors() as $floor) {
                foreach ($floor->getFlats() as $flat) {

                    if ($flat->getStatus() != 'sold_out') {
                        $flatCount++;
                    }
                }
            }
        }

        return $flatCount;
    }


    /**
     *  Сохранение кол-ва квартир, макс. этажей, подъездов в доме
     * @param Building $building
     * @param EntityManagerInterface $em
     */
    public function updateAggsDataInBuilding(Building $building, EntityManagerInterface $em)
    {
        $floorCount = 0;
        $flatCount = 0;

        if ($building->getBlocks()) {

            foreach ($building->getBlocks() as $block) {

                if ($block->getFloors()) {

                    foreach ($block->getFloors() as $floor) {

                        if ($floor->getFlats()) {

                            foreach ($floor->getFlats() as $flat) {

                                if ($flat->getStatus() != 'sold_out') {
                                    $flatCount++;
                                }
                            }
                        }
                    }

                    $floorCount = $floorCount > $block->getFloors()->count() ? $floorCount : $block->getFloors()->count();
                }
            }


            $building->setBlockCount( $building->getBlocks()->count() );
            $building->setFlatCount( $flatCount );
            $building->setFloorCount( $floorCount );

            $em->persist( $building );
            $em->flush();

        }
    }
}