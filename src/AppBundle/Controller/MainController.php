<?php

namespace AppBundle\Controller;

use Acme\Bundle\UserBundle\Entity\User;
use AppBundle\DBAL\Types\FlatDecorType;
use AppBundle\DBAL\Types\FlatStatusType;
use AppBundle\DBAL\Types\QuarterType;
use AppBundle\Entity\Building;
use AppBundle\Entity\City;
use AppBundle\Entity\Flat;
use AppBundle\Entity\Rate;
use AppBundle\EventListener\JmsSerializerListener;
use AppBundle\Form\FlatType;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Form\Factory\FactoryInterface;
use FOS\UserBundle\FOSUserEvents;
use JMS\Serializer\EventDispatcher\EventDispatcher;
use JMS\Serializer\EventDispatcher\PreSerializeEvent;
use JMS\SerializerBundle\JMSSerializerBundle;
use JMS\Serializer\Handler\HandlerRegistry;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sonata\UserBundle\Model\UserInterface;
use Sonata\UserBundle\Model\UserManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\HttpKernel;
use AppBundle\Utils\MainSearch;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class MainController extends Controller
{
    protected $connection;
    private $eventDispatcher;

    public function __construct(EventDispatcherInterface $eventDispatcher, Connection $connection)
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->connection = $connection;
    }

    /**
     * @Route("/", name="index")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, EntityManagerInterface $em, SessionInterface $session)
    {

        $buildings = $em->getRepository(Building::class)->findAll();
        $cities = $em->getRepository(City::class)->findAll();
        $deadlines = array();

        foreach ($buildings as $building) {
            $year = $building->getDeadlineYear()->format('Y');
            $quarter = $building->getDeadlineQuarter();
            $deadlines[$year][$quarter] = QuarterType::getReadableValue($quarter) . '' . $year . 'г.';
        }

        if ($session->get('register_success')) {
            $this->addFlash('register_success', 'Регистрация прошла успешно');
        }

        return $this->render(':main:index.html.twig', [
            'buildings' => $buildings,
            'cities' => $cities,
            'deadlines' => $deadlines,
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }


    /**
     * @Route("/search-result/", name="search_result", options={ "expose" = true } )
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function searchAction(Request $request, EntityManagerInterface $em)
    {
        $buildings = $em->getRepository(Building::class)->findAll();
        $cities = $em->getRepository(City::class)->findAll();

        $deadlines = $this->_getDeadlines($buildings);

        //$finder = $this->container->get('fos_elastica.finder.app.building');

        $search = new MainSearch();

        $searchResult = $search->searchFilter($request, $em);
        $breadcrumbs = $search->breadcrumbs($request, $em);

        return $this->render(':main:search_result.html.twig', [
            'buildings' => $searchResult,
            'cities' => $cities,
            'deadlines' => $deadlines,
            'breadcrumbs' => $breadcrumbs,
        ]);
    }

    /**
     * @Route("/building/{id}", name="show_building")
     * @param Building $building
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showBuildingAction(Building $building, Request $request, EntityManagerInterface $em)
    {
        $flatType = $this->createForm(FlatType::class);
        $buildings = $em->getRepository(Building::class)->findAll();
        $cities = $em->getRepository(City::class)->findAll();

        $deadlines = $this->_getDeadlines($buildings);
        $rc = $request->get("rC");
        $rcArr = explode(',', $rc);
        $request->query->remove("rC");
        $search = new MainSearch();
        $search->searchFilter($request, $em);
        $breadcrumbs = $search->breadcrumbs($request, $em);

        return $this->render(':main:building.html.twig', [
            'flatType' => $flatType->createView(),
            'building' => $building,
            'cities' => $cities,
            'deadlines' => $deadlines,
            'breadcrumbs' => $breadcrumbs,
            'rc' => $rcArr,
        ]);
    }

    /**
     * @Route("/profile-agent/", name="profile_agent")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function profileAgentAction(Request $request, EntityManagerInterface $em, UserManagerInterface $userManager)
    {
        $buildings = $em->getRepository(Building::class)->findAll();
        $cities = $em->getRepository(City::class)->findAll();

        $deadlines = $this->_getDeadlines($buildings);

        //$finder = $this->container->get('fos_elastica.finder.app.building');

        $search = new MainSearch();

        $searchResult = $search->searchFilter($request, $em);
        $breadcrumbs = $search->breadcrumbs($request, $em);

        //форма изменение пароля
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }
        $event = new GetResponseUserEvent($user, $request);
        $this->eventDispatcher->dispatch(FOSUserEvents::CHANGE_PASSWORD_INITIALIZE, $event);
        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }
        $form = $this->createFormBuilder($user)->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $event = new FormEvent($form, $request);
            $this->eventDispatcher->dispatch(FOSUserEvents::CHANGE_PASSWORD_SUCCESS, $event);
            $userManager->updateUser($user);
            if (null === $response = $event->getResponse()) {
                $url = $this->generateUrl('profile_agent');
                $response = new RedirectResponse($url);
            }
            $this->eventDispatcher->dispatch(FOSUserEvents::CHANGE_PASSWORD_COMPLETED, new FilterUserResponseEvent($user, $request, $response));
            return $response;
        }
        //форма изменения пароля

        return $this->render(':main:profile_agent.html.twig', [
            'buildings' => $searchResult,
            'cities' => $cities,
            'deadlines' => $deadlines,
            'breadcrumbs' => $breadcrumbs,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/profile-developer/", name="profile_developer")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function profileDeveloperAction(Request $request, EntityManagerInterface $em)
    {
        $buildings = $em->getRepository(Building::class)->findAll();
        $cities = $em->getRepository(City::class)->findAll();

        $deadlines = $this->_getDeadlines($buildings);

        //$finder = $this->container->get('fos_elastica.finder.app.building');

        $search = new MainSearch();

        $searchResult = $search->searchFilter($request, $em);
        $breadcrumbs = $search->breadcrumbs($request, $em);
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }
        $event = new GetResponseUserEvent($user, $request);
        $this->eventDispatcher->dispatch(FOSUserEvents::CHANGE_PASSWORD_INITIALIZE, $event);
        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }
        $form = $this->createFormBuilder($user)->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $event = new FormEvent($form, $request);
            $this->eventDispatcher->dispatch(FOSUserEvents::CHANGE_PASSWORD_SUCCESS, $event);
            $this->userManager->updateUser($user);
            if (null === $response = $event->getResponse()) {
                $url = $this->generateUrl('fos_user_profile_show');
                $response = new RedirectResponse($url);
            }
            $this->eventDispatcher->dispatch(FOSUserEvents::CHANGE_PASSWORD_COMPLETED, new FilterUserResponseEvent($user, $request, $response));
            return $response;
        }

        return $this->render('@FOSUser/ChangePassword/change_password.html.twig', [
            'buildings' => $searchResult,
            'cities' => $cities,
            'deadlines' => $deadlines,
            'breadcrumbs' => $breadcrumbs,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/search-map/", name="search_map")
     */
    public function searchMapAction(EntityManagerInterface $em)
    {
        $buildings = $em->getRepository(Building::class)->findAll();
        $cities = $em->getRepository(City::class)->findAll();

        $deadlines = $this->_getDeadlines($buildings);
        return $this->render(':main:search_map.html.twig', array(
            'cities' => $cities,
            'deadlines' => $deadlines,
        ));
    }


    /**
     * @Route("/api/flat/{id}", name="get_flat",  requirements={"id": "\d+"}, options={"expose": true});
     * @Method("GET")
     * @param Flat $flat
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getFlat(Flat $flat)
    {
        $layoutImg = '';
        $points =  '';

        if ($area = $flat->getArea()) {
            $points = $area ? $area->getPoints() : '';
            $image = $area ? $area->getLayout()->getImage() : '';
            if ($image) {
                $layoutImg = $this->get('sonata.media.provider.file')->generatePublicUrl($image, 'reference');
            }
        }

        $flat->setFloorNum($flat->getFloor()->getNum());
        $flat->setBlockName($flat->getFloor()->getBlock()->getName());
        $flat->setPriceMortgage($flat->getPriceMortgage() ? : $flat->getPriceTotal());
        $flat->setPriceInstallment($flat->getPriceInstallment() ? : $flat->getPriceTotal());

        $flat->setStatus( FlatStatusType::getReadableValue($flat->getStatus()) );
        $flat->setDecor( FlatDecorType::getReadableValue($flat->getDecor()) );

        $serializerBundle = $this->container->get('jms_serializer');
        $params = $serializerBundle->serialize($flat, 'json');

        $result = array(
            'flat' => array(
                'layoutImg' => $layoutImg,
                'points' => $points,
                'params' => $params,
            )
        );

        return $this->json($result);
    }



    /**
     * Формирование сроков сдачи для фильтра
     * @param array $buildings
     * @return array
     */
    private function _getDeadlines($buildings = array())
    {
        $deadlines = array();
        foreach ($buildings as $item) {
            $year = $item->getDeadlineYear()->format('Y');
            $quarter = $item->getDeadlineQuarter();
            $deadlines[$year][$quarter] = QuarterType::getReadableValue($quarter) . '' . $year . 'г.';
        }
        return $deadlines;
    }

    /**
     * @Route("/profile/{id}", name="edit_user",  requirements={"id": "\d+"}, options={"expose": true});
     * @Method("GET")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param User $user
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editUser(User $user, Request $request, EntityManagerInterface $em)
    {
        if($request->get('firstname')){
            $user->setFirstname($request->get('firstname'));
        }
        if($request->get('lastname')){
            $user->setLastname($request->get('lastname'));
        }
        if($request->get('email')){
            $user->setEmail($request->get('email'));
        }
        if($request->get('username')){
            $user->setUsername($request->get('username'));
            $user->setUsernameCanonical($request->get('username'));
        }
        if($request->get('phone')){
            $user->setPhone($request->get('phone'));
        }
        if($request->get('website')){
            $user->setWebsite($request->get('website'));
        }
        $em->persist($user);
        $em->flush();

//        var_dump($request->get('email'));die();
        return $this->redirectToRoute('profile_agent');
    }
    /**
     * @Route("/rates/", name="rate_list_personal")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function rateListPersonal(Request $request, EntityManagerInterface $em, UserManagerInterface $userManager)
    {
        $buildings = $em->getRepository(Building::class)->findAll();
        $cities = $em->getRepository(City::class)->findAll();

        $sql = "SELECT rate.id as rate_id, " .
            "rate.percent_profbrony as rate_percent_profbrony, " .
            "rate.percent_for_you as rate_percent_for_you, " .
            "rate.percent_max as rate_percent_max, " .
            "rate.percent_prof_parther as rate_percent_prof_parther, " .
            "rate.persent_dop as rate_persent_dop, " .
            "rate.percent_private as rate_percent_private, " .
            "developer.name as developer, " .
            "SUM(IF(object.percent_profbrony IS NULL,0,1)) as object_percent_profbrony, " .
            "SUM(IF(object.percent_for_you IS NULL,0,1)) as object_percent_for_you, " .
            "SUM(IF(object.percent_max IS NULL,0,1)) as object_percent_max, " .
            "SUM(IF(object.percent_prof_parther IS NULL,0,1)) as object_percent_prof_parther, " .
            "SUM(IF(object.percent_dop IS NULL,0,1)) as object_percent_dop, " .
            "SUM(IF(object.percent_private IS NULL,0,1)) as object_percent_private, " .
            "SUM(IF(building.percent_profbrony IS NULL,0,1)) as building_percent_profbrony, " .
            "SUM(IF(building.percent_for_you IS NULL,0,1)) as building_percent_for_you, " .
            "SUM(IF(building.percent_max IS NULL,0,1)) as building_percent_max, " .
            "SUM(IF(building.percent_prof_parther IS NULL,0,1)) as building_percent_prof_parther, " .
            "SUM(IF(building.percent_dop IS NULL,0,1)) as building_percent_dop, " .
            "SUM(IF(building.percent_private IS NULL,0,1)) as building_percent_private " .
            "FROM rate " .
            "LEFT JOIN developer ON (developer.id = rate.developer_id) " .
            "LEFT JOIN object ON (developer.id = object.developer_id) " .
            "LEFT JOIN building ON (object.id = building.object_id)  GROUP BY(rate_id);";
        $data = $this->connection->fetchAll($sql);
        if ($data === false) {
            return false;
        }
//        $rates = $em->getRepository(Rate::class)->findAll();
        $deadlines = $this->_getDeadlines($buildings);
        return $this->render(':main:rate_personal_list.html.twig', array(
            'cities' => $cities,
            'deadlines' => $deadlines,
            'rates' => $data
        ));
    }



}
