<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Area;
use AppBundle\Entity\Block;
use AppBundle\Entity\Building;
use AppBundle\Entity\Flat;
use AppBundle\Entity\Floor;
use AppBundle\Entity\FloorLayout;
use AppBundle\Form\BlockType;
use AppBundle\Form\FlatType;
use AppBundle\Utils\AggsHelper;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sonata\AdminBundle\Controller\CRUDController as Controller;

class CRUDController extends Controller
{

    /**
     * Шахматка
     * @param $id
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function boardAction($id, EntityManagerInterface $em)
    {

        ini_set('memory_limit', '512M');

        $request = Request::createFromGlobals();
        $building = $this->admin->getSubject();

        if (!$building) {
            throw new NotFoundHttpException(sprintf('Unable to find the building with id: %s', $id));
        }

        // Добавление подъезда
        $block = new Block();
        $blockForm = $this->createForm(BlockType::class, $block);
        $blockForm->handleRequest($request);

        if ($blockForm->isSubmitted() && $blockForm->isValid()) {

            $block = $blockForm->getData();
            $block->setBuilding($building);
            $em->persist($block);
            $em->flush();

            // Пересчет агрегаций в доме
            $aggsHelper = new AggsHelper();
            $aggsHelper->updateAggsDataInBuilding( $building, $em );

            return $this->redirectToRoute('admin_app_object_building_board', array(
                'id' => $building->getObject()->getId(),
                'childId' => $building->getId(),
            ));
        }

        // Добавление квартиры
        $flat = new Flat();
        $flatForm = $this->createForm(FlatType::class, $flat);
        $flatForm->handleRequest($request);

        if ($flatForm->isSubmitted() && $flatForm->isValid()) {

            $flat = $flatForm->getData();
            $post = $request->request->get('flat');
            $floor = $em->getRepository(Floor::class)->find($post['floor']);
            // Формируются в Entity\Flat
            $flat->setRoomType('');
            $flat->setPriceMeter(0);
            $flat->setFloor($floor);
            $em->persist($flat);
            $em->flush();

            // Пересчет агрегаций в доме
            $aggsHelper = new AggsHelper();
            $aggsHelper->updateAggsDataInBuilding( $building, $em );

            return $this->redirectToRoute('admin_app_object_building_board', array(
                'id' => $building->getObject()->getId(),
                'childId' => $building->getId(),
            ));
        }

        // Создание форм для всех квартир
        $flatForms = array();
        $blocks = $em->getRepository(Block::class)->findBy(array('building' => $building));
        foreach ($blocks as $block) {
            foreach ($block->getFloors() as $floor) {
                foreach($floor->getFlats() as $flat) {
                    $flatForms[$flat->getId()] = $this->get('form.factory')
                        ->createNamedBuilder('flatForm' . $flat->getId(), FlatType::class, $flat)
                        ->getForm()
                        ->createView();
                }
            }
        }


        return $this->render('AppBundle:Admin:board_edit.html.twig', array(
            'object' => $building,
            'blockForm' => $blockForm->createView(),
            'flatForm' => $flatForm->createView(),
            'flatForms' => $flatForms,
        ));
    }

    /**
     *              Обновление параметров квартиры
     * @param $id
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function updateFlatAction($id, EntityManagerInterface $em)
    {
        $request = Request::createFromGlobals();
        $params = $request->get('flatForm' . $id);
        $response = 'Bad. Flat is not updated!';
        if ($params) {
            $flat = $em->getRepository(Flat::class)->find($id);
            $flat->setNum($params['num']);
            $flat->setStudio(isset($params['studio']) ? : false);
            $flat->setAction(isset($params['action']) ? : false);
            $flat->setInvestorSales(isset($params['investorSales']) ? : false);
            $flat->setStatus($params['status'] ? : '');
            $flat->setDecor($params['decor'] ? : '');
            $flat->setRoomCount($params['roomCount'] ? : null);
            $flat->setStatus($params['status'] ? : null);
            $flat->setPriceMortgage($params['priceMortgage'] ? : null);
            $flat->setPriceInstallment($params['priceInstallment'] ? : null);
            $flat->setAreaTotal($params['areaTotal'] ? : null);
            $flat->setAreaLiving($params['areaLiving'] ? : null);
            $flat->setKitchenArea($params['kitchenArea'] ? : null);
            $flat->setBathroomArea($params['bathroomArea'] ? : null);
            $flat->setBathroomCount($params['bathroomArea'] ? : null);
            $flat->setBalconyArea($params['balconyArea'] ? : null);
            $flat->setBalconyCount($params['balconyCount'] ? : null);
            $flat->setLoggiaArea($params['loggiaArea'] ? : null);
            $flat->setLoggiaCount($params['loggiaCount'] ? : null);

            // Формируются в Entity\Flat

            // цены за все и м2
            $flat->setPrice($params['priceTotal']);

            // тип комнатности
            $flat->setRoomType();

            $em->persist($flat);
            $em->flush();
            $response = 'Ok. Flat updated.';

            // Пересчет агрегаций в доме
            $building = $flat->getFloor()->getBlock()->getBuilding();
            $aggsHelper = new AggsHelper();
            $aggsHelper->updateAggsDataInBuilding( $building, $em );

        }
        return new Response($response);
    }

    /**
     * Удаление подъезда
     * @param Block $block
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteBlockAction(Block $block, EntityManagerInterface $em)
    {

        $em->remove($block);
        $em->flush();

        // Пересчет агрегаций в доме
        $building = $block->getBuilding();
        $aggsHelper = new AggsHelper();
        $aggsHelper->updateAggsDataInBuilding( $building, $em );

        return $this->redirectToRoute('admin_app_building_board', ['id' => $building->getId()]);
    }

    /**
     * Удаление этажа
     * @param Floor $floor
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteFloorAction(Floor $floor, EntityManagerInterface $em)
    {
        $em->remove($floor);
        $em->flush();

        // Пересчет агрегаций в доме
        $building = $floor->getBlock()->getBuilding();
        $aggsHelper = new AggsHelper();
        $aggsHelper->updateAggsDataInBuilding( $building, $em );

        return $this->redirectToRoute('admin_app_building_board', ['id' => $building->getId()]);
    }

    /**
     * Удаление квартиры
     * @param Flat $flat
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteFlatAction(Flat $flat, EntityManagerInterface $em)
    {
        $em->remove($flat);
        $em->flush();

        // Пересчет агрегаций в доме
        $building = $flat->getFloor()->getBlock()->getBuilding();
        $aggsHelper = new AggsHelper();
        $aggsHelper->updateAggsDataInBuilding( $building, $em );

        return $this->redirectToRoute('admin_app_building_board', ['id' => $building->getId()]);
    }

    public function areasAction($id)
    {
        $floorLayout = $this->admin->getSubject();
        return $this->render('AppBundle:Admin:areas_edit.html.twig', array(
            'object' => $floorLayout
        ));
    }


    /**
     * Массив этажей для выпадающего списка в редактировании разметки планировки
     * @param FloorLayout $floorLayout
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function floorsAction(FloorLayout $floorLayout)
    {
        $response = array();
        $floorsArray = array();
        $block = $floorLayout->getBlock();
        $floors = $block->getFloors();
        foreach ($floors as $floor) {

            $flatsArray = array();
            $selected = false;
            $active = false;

            if ($floor->getLayout()) {
                if ($floor->getLayout()->getId() == $floorLayout->getId()) {
                    $selected = true;
                    $active = true;
                } else {
                    $active = false;
                }
            } else {
                $active = true;
            }


            foreach ($floor->getFlats() as $flat) {
                $flatsArray[$flat->getNum()] = $flat->getId();
            }

            ksort($flatsArray);

            $floorsArray[$floor->getNum()] = array(
                'floor' => array(
                    'id' => $floor->getId(),
                    'num' => $floor->getNum(),
                    'selected' => $selected,
                    'active' => $active,
                    'flats' => $flatsArray
                ),
            );
        }

        $response = array_reverse($floorsArray);
        return $this->json($response);
    }

    /**
     * Добавление планировки к этажу
     * @param Floor $floor
     * @param $layoutId
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function addLayoutAction(Floor $floor, $layoutId, EntityManagerInterface $em)
    {
        $floorLayout = $em->getRepository(FloorLayout::class)->find($layoutId);
        $floor->setLayout($floorLayout);
        $em->persist($floor);
        $em->flush();
        $flats = $floor->getFlats()->toArray();
        $flatsArr = array();
        foreach ($flats as $flat) {
            $flatsArr[] = array(
                'id' => $flat->getId(),
                'num' => $flat->getNum(),
            );
        }
        $result = array(
            'floor' => array(
                'id' => $floor->getId(),
                'num' => $floor->getNum(),
                'flats' => $flatsArr,
            )
        );

        return $this->json($result);
    }

    /**
     * Удаление планировки у этажа
     * @param Floor $floor
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function deleteLayoutAction(Floor $floor, EntityManagerInterface $em)
    {
        if ($flats = $floor->getFlats()) {
            foreach ($flats as $flat) {
                $flat->setArea(null);
            }
        }

        $floor->setLayout(null);
        $em->persist($floor);
        $em->flush();
        $result = array(
            'floorId' => $floor->getId()
        );
        return $this->json($result);
    }


    /**
     * Сохраненние координат области на фото планировки
     * @param FloorLayout $floorLayout
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function areaCreateAction(FloorLayout $floorLayout, Request $request, EntityManagerInterface $em)
    {
        $result = array();
        $result['success'] = false;
        $points = $request->request->get('points');

        if ($points) {

            $area = new Area();
            $area->setPoints($points);
            $area->setLayout($floorLayout);

            $em->persist($area);
            $em->flush();

            $result['success'] = true;
            $result['area'] = array(
                'id' => $area->getId(),
                'layout_id' => $floorLayout->getId(),
                'points' => json_decode($points)
            );
        }

        return $this->json($result);
    }


    /**
     * Массив областей для фото планировки
     * @param FloorLayout $floorLayout
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */

    public function areaListAction(FloorLayout $floorLayout, Request $request, EntityManagerInterface $em)
    {
        $result = array();
        $areasArr = array();
        $result['success'] = false;

        $areas = $floorLayout->getAreas();

        if (count($areas)) {
            foreach ($areas as $area) {
                $areasArr[] = array(
                    'id' => $area->getId(),
                    'layout_id' => $floorLayout->getId(),
                    'points' => json_decode($area->getPoints(), true)
                );
            }
            $result['success'] = true;
            $result['areas'] = $areasArr;
        }

        return $this->json($result);
    }



}