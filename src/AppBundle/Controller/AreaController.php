<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Area;
use AppBundle\Entity\Flat;
use Doctrine\ORM\EntityManagerInterface;
use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpFoundation\Request;

class AreaController extends Controller
{
    /**
     * @param Area $area
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getAction(Area $area)
    {
        $result = array();

        if ($area) {
            $flats = $area->getFlats();
            $flatsArray = array();

            if ($flats) {
                foreach ($flats as $flat) {
                    $flatsArray[] = array(
                        'id' => $flat->getId(),
                        'num' => $flat->getNum(),
                    );
                }
            }
            $result = array(
                'id' => $area->getId(),
                'flats' => $flatsArray,
            );
        }

        return $this->json($result);
    }

    /**
     * Добавление квартир к области на фото планировки
     * @param Area $area
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function flatAddAction(Area $area, Request $request, EntityManagerInterface $em)
    {
        $result = array();
        $flats = $request->request->get('flats');

        if ($flats) {

            $flatsOld = $em->getRepository(Flat::class)->findBy(array('area' => $area));
            foreach ($flatsOld as $flatOld) {
                $flatOld->setArea(null);
                $em->persist($flatOld);
                $em->flush();
            }

            foreach ($flats as $flat) {
                $flat = $em->getRepository(Flat::class)->findOneBy(array('id' => $flat));
                if ($flat && !$flat->getArea()) {
                    $flat->setArea($area);
                    $em->persist($area);
                    $em->flush();
                }
            }
        }

        return $this->json($result[] = 'OK');
    }

    public function removeAction(Request $request, EntityManagerInterface $em)
    {
        $id = $request->request->get('id');
        $success = false;

        if ($id) {
            try {
                $area = $em->getRepository(Area::class)->findOneBy(['id' => $id]);
                $em->remove($area);
                $em->flush();
                $success = true;
            } catch (\Exception $e) {

            }
        }

        $result = array(
            'success' => $success
        );

        return $this->json($result);
    }
}