<?php

namespace AppBundle\Controller;

use AppBundle\DBAL\Types\FlatDecorType;
use AppBundle\DBAL\Types\FlatStatusType;
use AppBundle\Entity\Block;
use AppBundle\Entity\Building;
use AppBundle\Entity\Flat;
use AppBundle\Entity\Floor;
use AppBundle\Entity\Object;
use AppBundle\Utils\AggsHelper;
use Doctrine\ORM\EntityManagerInterface;
use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpFoundation\Request;

class ImportController extends Controller
{
    /**
     * Маппинг паметров домов объекта
     * @param Object $object
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function importMappingAction(Object $object, EntityManagerInterface $em)
    {
        $filePath = '.' . $this->get('sonata.media.provider.file')->generatePublicUrl($object->getImportFile(), 'reference');
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject($filePath);
        $table = array();
        $rowIndex = 0;
        $columnsNum = 0;
        foreach ($phpExcelObject->getWorksheetIterator() as $worksheet) {
            foreach ($worksheet->getRowIterator() as $row) {
                if (!$this->_isEmptyRow($row)) {
                    $rowIndex = $row->getRowIndex();
                    $cellIterator = $row->getCellIterator();
                    $i = 0;
                    foreach ($cellIterator as $cell) {
                        if ($rowIndex == 1 && $cell->getValue() != null) {
                            $columnsNum++;
                            $table[$rowIndex][$cell->getColumn()]['value'] = $cell->getValue();
                        }
                        if (!is_null($cell) && $rowIndex > 1 && $i < $columnsNum) {
                            $value =  $cell->getCalculatedValue() ? : $cell->getValue();
                            if (filter_var($value, FILTER_VALIDATE_INT)) {
                                $table[$rowIndex][$cell->getColumn()]['value'] = $value;
                            } elseif (filter_var($value, FILTER_VALIDATE_FLOAT)) {
                                $table[$rowIndex][$cell->getColumn()]['value'] = number_format($value, 2, '.', '');
                            } else {
                                $table[$rowIndex][$cell->getColumn()]['value'] = $value;
                            }
                            $i++;
                        }
                    }
                } else {
                    break;
                }
            }
        }

        $flatParams = array(
            'Num' => 'required',
            'Block' => '',
            'Floor' => 'required',
            'Room count' => 'required',
            'Studio' => '',
            'Status' => '',
            'Decor' => '',
            'Price total' => '',
            'Price meter' => '',
            'Price mortgage' => '',
            'Price installment' => '',
            'Area total' => 'required',
            'Area living' => '',
            'Kitchen area' => '',
            'Bathroom count' => '',
            'Bathroom area' => '',
            'Balcony count' => '',
            'Balcony area' => '',
            'Loggia count' => '',
            'Loggia area' => '',
            'Building' => '',
        );

        return $this->render('AppBundle:Admin:object_import_mapping.html.twig', array(
            'object' => $object,
            'table' => $table,
            'flatParams' => $flatParams,
        ));
    }

    public function importAction(Object $object, Request $request, EntityManagerInterface $em)
    {
        $params = $request->get('params');
        $importBuildings = $request->get('importBuildings');
        $mappedArray = $object->getImportMapping();
        $buildingId = null;
        $buildings = array();

        // Если есть карта импортируемых домов, то начинаем перебор всех квартир с файла и запись в базу

        if (isset($importBuildings) && !empty($importBuildings)) {

            $mappedParams = $mappedArray['values'];

            // Формирование имен атрибутов
            foreach ($mappedParams as $mappedParamKey => $mappedParamValue) {
                $mappedParam = array();
                foreach ($mappedParams[$mappedParamKey] as $k => $v) {
                    $paramName = explode('_', lcfirst($k));
                    $paramName[1] = isset($paramName[1]) ? ucfirst($paramName[1]) : '';
                    $paramName = implode($paramName);
                    $mappedParam[$paramName] = $v;
                    unset($mappedParam[$k]);
                }
                $mappedParams[$mappedParamKey] = $mappedParam;
                unset($mappedParam);
            }


            // Создается массив соответствий импортируемых домов с домами в базе
            // Если домов при импорте более одного, то выбираем их названия из документов,
            // Иначе присваиваем название текущего дома в базе
            $buildingNames = array();
            if (count($importBuildings) > 1) {
                foreach ($importBuildings as $k => $v) {
                    $buildings[] = array(
                        'id' => $v,
                        'name' => $mappedParams[$k]['building'],
                        'blocks' => array(),
                    );
                }
            } else {
                $buildings[] = array(
                    'id' => $importBuildings[0],
                    'name' => '',
                    'blocks' => array()
                );
            }

            // Формирование дерева объекта
            foreach ($mappedParams as $params) {
                if ($params['num']) {
                    $buildingArrayKey = array_search($params['building'], array_column($buildings, 'name'));
                    $buildingArrayKey = $buildingArrayKey ? : 0;
                    $blockName = key_exists('block', $params) && !empty($params['block']) ? $params['block'] : '1';
                    if (array_search($blockName, array_column($buildings[$buildingArrayKey]['blocks'], 'name')) === false) {
                        $buildings[$buildingArrayKey]['blocks'][] = array(
                            'name' => $blockName,
                            'floors' => array(),
                        );
                    }
                    $priceTotal = 0;
                    $priceMeter = 0;
                    if (key_exists('priceTotal', $params) ) {
                        $priceTotal = str_replace(' ', '', $params['priceTotal']);
                    } elseif(key_exists('priceMeter', $params)) {
                        $priceMeter = str_replace(' ', '', $params['priceMeter']);
                    }

                    $blockArrayKey = array_search($blockName, array_column($buildings[$buildingArrayKey]['blocks'], 'name'));
                    $buildings[$buildingArrayKey] ['blocks'] [$blockArrayKey] ['floors'] [$params['floor']] ['flats'][] = array(
                        'num' => $params['num'],
                        'roomCount' => $params['roomCount'],
                        'decor' => FlatDecorType::getEnumByValue(isset($params['decor']) ? self::_mb_ucfirst($params['decor']) : ''),
                        'status' => FlatStatusType::getEnumByValue(isset($params['status']) ? self::_mb_ucfirst($params['status']) : ''),
                        'studio' => $params['studio'] ? true : false,
                        'priceTotal' => $priceTotal,
                        'priceMeter' => $priceMeter,
                        'priceMortgage' =>  isset($params['priceMortgage']) ? $params['priceMortgage'] : $priceTotal,
                        'priceInstallment' => isset($params['priceInstallment']) ? $params['priceInstallment'] : $priceTotal,
                        'areaTotal' => isset($params['areaTotal']) ? $params['areaTotal'] : 0,
                        'areaLiving' => isset($params['areaLiving']) ? $params['areaLiving'] : 0,
                        'kitchenArea' => isset($params['kitchenArea']) ? $params['kitchenArea'] : 0,
                        'bathroomCount' => isset($params['bathroomCount']) ? $params['bathroomCount'] : 0,
                        'bathroomArea' => isset($params['bathroomArea']) ? $params['bathroomArea'] : 0,
                        'loggiaCount' => isset($params['loggiaCount']) ? $params['loggiaCount'] : 0,
                        'loggiaArea' => isset($params['loggiaArea']) ? $params['loggiaArea'] : 0,
                        'balconyCount' => isset($params['balconyCount']) ? $params['balconyCount'] : 0,
                        'balconyArea' => isset($params['balconyArea']) ? $params['balconyArea'] : 0,
                    );
                }
            }
            unset($blockArrayKey, $blockName, $buildingArrayKey, $buildingId, $buildingNames, $importBuildings, $k, $mappedArray, $mappedParamKey,
                $mappedParams, $mappedParamValue, $mappedParamKey, $paramName, $params, $v, $priceTotal);

            // Сохранение данных в БД

            foreach ($buildings as $buildingArray) {
                $building = $em->getRepository(Building::class)->find($buildingArray['id']);

                foreach ($buildingArray['blocks'] as $blockArray) {

                    $block = $em->getRepository(Block::class)->findOneBy(array('building' => $building, 'name' => $blockArray['name']));

                    if (!$block) {
                        $block = new Block();
                        $block->setBuilding($building);
                        $block->setName($blockArray['name']);
                        $em->persist($block);
                    }

                    foreach ($blockArray['floors'] as $floorArrayKey => $floorArrayValue) {

                        $floor = $em->getRepository(Floor::class)->findOneBy(array('block' => $block, 'num' => $floorArrayKey));

                        if (!$floor) {
                            $floor = new Floor();
                            $floor->setBlock($block);
                            $floor->setNum($floorArrayKey);
                            $em->persist($floor);
                        }


                        foreach ($floorArrayValue['flats'] as $flatArray) {

                            $flat = $em->getRepository(Flat::class)->findOneBy(array('floor' => $floor, 'num' => $flatArray['num']));

                            if (!$flat) {
                                $flat = new Flat();
                                $flat->setNum($flatArray['num']);
                                $flat->setFloor($floor);
                            }

                            $flat->setNum($flatArray['num']);
                            $flat->setStudio($flatArray['studio']);
                            $flat->setDecor($flatArray['decor']);
                            $flat->setStatus($flatArray['status']);
                            $flat->setRoomCount($flatArray['roomCount']);
                            $flat->setAreaTotal($flatArray['areaTotal']);
                            $flat->setAreaLiving($flatArray['areaLiving']);
                            $flat->setKitchenArea($flatArray['kitchenArea']);
                            $flat->setBathroomArea($flatArray['bathroomArea']);
                            $flat->setBathroomCount($flatArray['bathroomCount']);
                            $flat->setBalconyArea($flatArray['balconyArea']);
                            $flat->setBalconyCount($flatArray['balconyCount']);
                            $flat->setLoggiaArea($flatArray['loggiaArea']);
                            $flat->setLoggiaCount($flatArray['loggiaCount']);
                            $flat->setInvestorSales($flat->getInvestorSales() ? : 0);
                            $flat->setAction($flat->getAction() ? : 0);


                            $price = $flatArray['priceMeter'];
                            $isTotal = false;

                            if (!$price) {
                                $price = $flatArray['priceTotal'];
                                $isTotal = true;
                            }

                            // Формируются в Entity\Flat

                            // цены за все и м2
                            $flat->setPrice($price, $isTotal);

                            // тип комнатности
                            $flat->setRoomType();

                            $em->persist($flat);
                        }
                    }
                }

                $em->persist($building);
            }

            $em->flush();

            // Пересчет агрегаций в доме
            if ($object->getBuildings()) {
                foreach ($object->getBuildings() as $building) {
                    $aggsHelper = new AggsHelper();
                    $aggsHelper->updateAggsDataInBuilding( $building, $em );
                }
            }

            $this->addFlash('success', 'Данные успешно имортированы');

            return $this->redirectToRoute('admin_app_object_edit', array('id' => $object->getId()));

        }


        $paramValues = array();
        $params = $request->get('params');

        if (!$importBuildings && is_array($params)) {
            $filePath = '.' . $this->get('sonata.media.provider.file')->generatePublicUrl($object->getImportFile(), 'reference');
            $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject($filePath);
            if (!empty($params)) {
                $rowIndex = 0;
                $columnsNum = 0;
                foreach ($phpExcelObject->getWorksheetIterator() as $worksheet) {
                    foreach ($worksheet->getRowIterator() as $rowIndex => $row) {
                        if (!$this->_isEmptyRow($row)) {
                            $cellIterator = $row->getCellIterator();
                            $i = 0;
                            foreach ($cellIterator as $cellIndex => $cell) {
                                if ($rowIndex == 1 && $cell->getValue() != null) {
                                    $columnsNum++;
                                }

                                if (!is_null($cell) && $rowIndex > 1 && $i < $columnsNum) {
                                    $param = array_search($cellIndex, $params);
                                    if ($param) {
                                        $value = $cell->getCalculatedValue() ? : $cell->getValue();
                                        if (filter_var($value, FILTER_VALIDATE_INT)) {
                                            $paramValues[$rowIndex - 2][$param] = $value;
                                        } elseif (filter_var($value, FILTER_VALIDATE_FLOAT)) {
                                            $paramValues[$rowIndex - 2][$param] = number_format($value, 2, '.', '');
                                        } else {
                                            $paramValues[$rowIndex - 2][$param] = $value;
                                        }
                                    }
                                    $i++;
                                }
                            }
                        } else {
                            break;
                        }
                    }
                }
            }

            if (!empty($paramValues)) {
                $object->setImportMapping(array(
                    'map' => $params,
                    'values' => $paramValues,
                ));
                $em->persist($object);
                $em->flush();
            }
            $mappedArray = $object->getImportMapping();
            unset($cell, $cellIndex, $cellIterator, $filePath, $helper, $phpExcelObject, $row, $rowIndex, $k, $v, $worksheet);
        }

        $importBuildings = array();
        if ($mappedArray['values']) {
            foreach ($mappedArray['values'] as $flat) {
                if (key_exists('Building', $flat)) {
                    $importBuildings[] = $flat['Building'];
                }
            }
            $importBuildings = array_unique($importBuildings);

            // Если в списке нет домов, очищаем мыссив
            if (count($importBuildings) == 1 && $importBuildings[0] === null) {
                $importBuildings = array();
            }

            if (empty($importBuildings)) {
                $importBuildings[] = '-';
            }
        }


        return $this->render('AppBundle:Admin:object_import.html.twig', array(
            'object' => $object,
            'importBuildings' => $importBuildings,
        ));
    }

    /**
     * Проверка пустых рядов
     * @param $row
     * @return bool
     */
    private function _isEmptyRow($row)
    {
        foreach ($row->getCellIterator() as $cell) {
            if ($cell->getValue()) {
                return false;
            }
        }

        return true;
    }

    /**
     * Truncate a float number, example: <code>truncate(-1.49999, 2); // returns -1.49
     * truncate(.49999, 3); // returns 0.499
     * </code>
     * @param float $val Float number to be truncate
     * @param int $f Number of precision
     * @return float
     */
    function _truncate($val, $f="0")
    {
        if(($p = strpos($val, '.')) !== false) {
            $val = floatval(substr($val, 0, $p + 1 + $f));
        }
        return $val;
    }

    private static function _mb_ucfirst($str) {
    $fc = mb_strtoupper(mb_substr($str, 0, 1));
    return $fc.mb_substr($str, 1);
}
}