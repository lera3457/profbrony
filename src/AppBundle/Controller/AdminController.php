<?php
/**
 * Created by PhpStorm.
 * User: che
 * Date: 02.04.18
 * Time: 15:46
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Building;
use AppBundle\Entity\Object;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\EventDispatcher\EventDispatcher;
use JMS\Serializer\EventDispatcher\PreSerializeEvent;
use JMS\SerializerBundle\JMSSerializerBundle;
use JMS\Serializer\Handler\HandlerRegistry;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\HttpKernel;
use AppBundle\Utils\MainSearch;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class AdminController extends Controller
{
    protected $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @Route("/persentTable", name="persentTable")
     * @Method("GET")
     */
    public function persentTable(Request $request, EntityManagerInterface $em)
    {
       /* $org = $this->getDoctrine()->getRepository(Object::class)->findAll();
        $org1 = $this->getDoctrine()->getRepository(Building::class)->findAll();
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();

        $normalizer->setCircularReferenceHandler(function ($org1) {
            return $org1->getName();
        });

        $serializer = new Serializer(array($normalizer), array($encoder));
        var_dump($serializer->serialize($org, 'json'));*/

      /*  $repository = $this->getDoctrine()->getRepository(Object::class)->findAll();
        // $product = $repository->findBy(array('developer' => $id));
        // $building = $repository->findAll();
            return $this->json($repository);*/
        /*$buildings = $em->getRepository(Object::class)->findBy(
            array("")
        );*/
      /*  $query = $em->createQuery(
            'SELECT p, c FROM AppBundle:Building p
        JOIN p.object c
        WHERE p.id = :id'
        )->setParameter('id', $id);
        var_dump($query->getResult());*/


        $id = $request->get('id_d');
        $sql = "SELECT object.id, object.name, " .
            "object.percent_profbrony, " .
            "object.percent_for_you, " .
            "object.percent_max, " .
            "object.percent_prof_parther, " .
            "object.percent_dop, " .
            "object.percent_private " .
            "FROM object " .
            "WHERE object.developer_id = ". $id . " ; ";
        $data = $this->connection->fetchAll($sql, array("code" => $id));
        if ($data === false) {
            return false;
        }

        return $this->json($data);
    }


    /**
     * @Route("/persentTable/update", name="updatePersent")
     *
     */
    public function updatePersentTable(Request $request){

        $column = $request->get('name');
        $newValue = $request->get('value');
        $id = $request->get('pk');
        //var_dump($id, $column, $newValue);

        $sql = "UPDATE object SET ".$column." = ".$newValue." where id = ".$id;
        $paramValues = array('col' => $column, 'val' => $newValue, 'id' => $id);
        $paramTypes = array('col' => \PDO::PARAM_STR, 'val' => \PDO::PARAM_STR, 'id' => \PDO::PARAM_STR);
//        $this->conn->executeUpdate($sql, $paramValues, $paramTypes);
        $this->connection->executeUpdate($sql, $paramValues, $paramTypes);
        return new JsonResponse($newValue, 200);
    }

    /**
     * @Route("/persentTableBuilding", name="persentTableBuilding")
     * @Method("GET")
     */
    public function persentTableBuilding(Request $request, EntityManagerInterface $em)
    {
        $id = $request->get('id_o');
        $sql = "SELECT building.id, building.name, " .
            "building.percent_profbrony, " .
            "building.percent_for_you, " .
            "building.percent_max, " .
            "building.percent_prof_parther, " .
            "building.percent_dop, " .
            "building.percent_private " .
            "FROM building " .
            "WHERE building.object_id = ". $id . " ; ";
        $data = $this->connection->fetchAll($sql, array("code" => $id));
        if ($data === false) {
            return false;
        }

        return $this->json($data);
    }

    /**
     * @Route("/persentTable/updateBuild", name="updatePersentBuild")
     *
     */
    public function updatePersentBuild(Request $request){

        $column = $request->get('name');
        $newValue = $request->get('value');
        $id = $request->get('pk');
        //var_dump($id, $column, $newValue);

        $sql = "UPDATE building SET ".$column." = ".$newValue." where id = ".$id;
        $paramValues = array('col' => $column, 'val' => $newValue, 'id' => $id);
        $paramTypes = array('col' => \PDO::PARAM_STR, 'val' => \PDO::PARAM_STR, 'id' => \PDO::PARAM_STR);
//        $this->conn->executeUpdate($sql, $paramValues, $paramTypes);
        $this->connection->executeUpdate($sql, $paramValues, $paramTypes);
        return new JsonResponse($newValue, 200);
    }

}