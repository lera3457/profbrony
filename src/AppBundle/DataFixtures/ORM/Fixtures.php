<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Building;
use AppBundle\Entity\City;
use AppBundle\Entity\Developer;
use AppBundle\Entity\District;
use AppBundle\Entity\Object;
use AppBundle\Entity\Region;
use AppBundle\Entity\Street;
use AppBundle\Entity\StreetType;
use AppBundle\Entity\SubDistrict;
use Application\Sonata\UserBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Provider\DateTime;

class Fixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        $materials = array('brick', 'monolith', 'panel');
        $type = array('comfort', 'comfort_plus', 'business', 'economy', 'elite');
        $payment = array('installment', 'mortgage', 'military_mortgage');

//        for ($i = 1; $i <= 5; $i++) {
//
//            $developer = new Developer();
//            $developer->setName('Застройщик ' . $i);
//            $manager->persist($developer);
//
//            $object = new Object();
//            $objectTypes = array('complex', 'district');
//            $object->setName('Объект ' . $i);
//            $object->setType($objectTypes[rand(0, 1)]);
//            $object->setDeveloper($developer);
//            $manager->persist($object);
//
//            for ($o = 1; $o <= rand(1, 3); $o++) {
//                $building = new Building();
//                $building->setName('Дом ' . $i);
//                $building->setObject($object);
//                $building->setDateStart(DateTime::dateTime('now'));
//                $building->setDeadlineYear(DateTime::dateTime('now'));
//                $building->setDeadlineQuarter('quarter' . rand(1, 4));
//                $building->setIsReady(rand(0, 1));
//                $building->setMaterialType($materials[array_rand($materials)]);
//                $building->setType($type[array_rand($type)]);
//                $building->setPaymentType($payment[array_rand($payment)]);
//                $manager->persist($building);
//            }
//
//
//
//
//
//            $region = new Region();
//            $region->setName('Регион ' . $i);
//            $manager->persist($region);
//
//            for ($j = 1; $j <= 5; $j++) {
//                $city = new City();
//                $city->setRegion($region);
//                $city->setName('Город ' . $i . '.' . $j);
//                $manager->persist($city);
//
//                for ($y = 1; $y <= 3; $y++) {
//                    $district = new District();
//                    $district->setName('Район ' . $j . '.' . $y);
//                    $district->setCity($city);
//                    $manager->persist($district);
//
//                    for ($z = 1; $z <= 5; $z++) {
//
//
//                        for($x = 1; $x <= 5; $x++) {
//                            $subDistrict = new SubDistrict();
//                            $subDistrict->setName('Микрорайон ' . $z . '.' . $x);
//                            $subDistrict->setDistrict($district);
//                            $manager->persist($subDistrict);
//                        }
//
//                        $street = new Street();
//                        $street->setName('Улица ' . $y . '.' . $z);
//                        $street->setDistrict($district);
//                        $manager->persist($street);
//                    }
//                }
//            }
//        }


        $streetTypes = array(
            'улица' => 'ул.',
            'проспект' => 'пр-т.',
            'бульвар' => 'бул.',
            'село' => 'с.',
            'деревня' => 'д.',
            'переулок' => 'пер.',
            'площадь' => 'пл.',
        );

        foreach ($streetTypes as $k => $v) {
            $streetType = new StreetType();
            $streetType->setName($k);
            $streetType->setNameAbbreviated($v);
            $manager->persist($streetType);
        }


        $user = new User();
        $user->setUsername('test');
        $user->setUsernameCanonical('test');
        $user->setEmail('test@test.com');
        $user->setEmailCanonical('test@test.com');
        $user->setEnabled(1);
        $user->setSalt('bagyBeOI8u31I3OsDpFo1mqIc8vqAH9nVhl9BR.5WyQ');
        $user->setPassword('bfngX/VgNC8mNXPFpZd2OjMuRFldIhXXiTGUU9CN9qNMAyOh7D9bL8Cid0lrV/v2irZVran1MmctF0hO9xJQYQ==');
        $user->setRoles(unserialize('a:1:{i:0;s:10:"ROLE_ADMIN";}'));
        $user->setCreatedAt(DateTime::dateTime('now'));
        $user->setGender('u');
        $manager->persist($user);

        $manager->flush();
    }
}