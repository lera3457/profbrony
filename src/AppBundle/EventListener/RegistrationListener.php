<?php

namespace AppBundle\EventListener;


use Doctrine\ORM\EntityManagerInterface;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\RouterInterface;

class RegistrationListener extends Controller implements EventSubscriberInterface
{

    private $router;
    /**
     * @var SessionInterface
     */
    private $session;

    public function __construct(RouterInterface $router, SessionInterface $session)
    {
        $this->router = $router;
        $this->session = $session;
    }

    public function onRegisterSuccess(FormEvent $event)
    {
        $this->session->set('register_success', '1');
        $uri = $this->router->generate('index');
        $response = new RedirectResponse($uri);
        $event->setResponse($response);
    }

    public function onRegisterCompleted(FilterUserResponseEvent $event)
    {
        $event->stopPropagation();
    }

    public function onRegisterConfirmed(FilterUserResponseEvent $event)
    {
        $event->stopPropagation();
    }

    public static function getSubscribedEvents()
    {
        return [
            FOSUserEvents::REGISTRATION_SUCCESS => 'onRegisterSuccess',
            FOSUserEvents::REGISTRATION_COMPLETED => 'onRegisterCompleted',
            FOSUserEvents::REGISTRATION_CONFIRMED => 'onRegisterConfirmed',
        ];
    }

}