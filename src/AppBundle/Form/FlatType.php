<?php

namespace AppBundle\Form;

use AppBundle\DBAL\Types\FlatDecorType;
use AppBundle\DBAL\Types\FlatStatusType;
use AppBundle\Entity\Flat;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FlatType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('num')
            ->add('roomCount', ChoiceType::class, array(
                'choices' => array(1=>1, 2=>2, 3=>3, 4=>4, 5=>5),
            ))
            ->add('status', ChoiceType::class, array(
                'choices' => FlatStatusType::getChoices(),
            ))
            ->add('decor', ChoiceType::class, array(
                'choices' => FlatDecorType::getChoices(),
            ))
            ->add('studio', CheckboxType::class, array('required' => false))
            ->add('action', CheckboxType::class, array('required' => false))
            ->add('investorSales', CheckboxType::class, array('required' => false))
            ->add('priceMeter', null, array(
                'disabled' => true
            ))
            ->add('priceTotal')
            ->add('areaTotal')
            ->add('priceMortgage', null, array('required' => false))
            ->add('priceInstallment', null, array('required' => false))
            ->add('areaLiving', null, array('required' => false))
            ->add('kitchenArea', null, array('required' => false))
            ->add('bathroomCount', null, array('required' => false))
            ->add('bathroomArea', null, array('required' => false))
            ->add('balconyCount', null, array('required' => false))
            ->add('balconyArea', null, array('required' => false))
            ->add('loggiaCount', null, array('required' => false))
            ->add('loggiaArea', null, array('required' => false))
            ->add('floor', HiddenType::class)
            ->add('roomType',HiddenType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Flat::class
        ));
    }
}