<?php
/**
 * Created by PhpStorm.
 * User: che
 * Date: 10.03.18
 * Time: 20:59
 */

namespace AppBundle\Admin;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Route\RouteCollection;

class RateAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('developer', 'sonata_type_model', array(
                'property' => 'name',
            ))
            ->add('percent_profbrony', null, array('required' => false, 'label' => 'Для Профброни, %'))
            ->add('percent_for_you', null, array('required' => false, 'label' => 'Для агенств, %'))
            ->add('percent_max', null, array('required' => false, 'label' => 'Максимально возм., %' ))
            ->add('percent_prof_parther', null, array('required' => false, 'label' => 'Для партнеров, %'))
            ->add('percent_private', null, array('required' => false, 'label' => 'Для частных агентов, %'))
            ->add('persent_dop', null, array('required' => false, 'label' => 'Дополнительные условия'))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('developer', null, array(
            'label' => 'Застройщик'
        ));
    }
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('developer', null, array(
                'label' => 'Застройщик'
            ))
            ->add('persent_object', 'text', array(
                'editable' => true, 'label' => 'Спецпредложения по базовым %'
            ))
            ->add('percent_profbrony', 'text', array(
                'editable' => true, 'label' => 'Для Профброни, %'
            ))
            ->add('percent_for_you', 'text', array(
                'label' => 'Для агенств, %', 'editable' => true
            ))
            ->add('percent_max', 'text', array(
                'label' => 'Максимально возм., %', 'editable' => true
            ))
            ->add('percent_prof_parther', 'text', array(
                'label' => 'Для партнеров, %', 'editable' => true
            ))
            ->add('percent_private', 'text', array(
                'label' => 'Для частных агентов, %', 'editable' => true, 'sortable' => true
            ))
            ->add('persent_dop', 'textarea', array(
                'label' => 'Дополнительные условия', 'editable' => true
            ))
        ;
    }

}