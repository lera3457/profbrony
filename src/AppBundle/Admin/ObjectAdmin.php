<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Route\RouteCollection;

class ObjectAdmin extends AbstractAdmin
{
    protected $parentAssociationMapping = 'developer';

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', 'text')
            ->add('type')
            ->add('developer', 'sonata_type_model', array(
                'property' => 'name',
            ))
            ->add('address', 'sonata_type_model', array(
                'property' => 'name',
            ))
            ->add('importFile', 'sonata_type_model_list',
                array(

                ),
                array(
                    'link_parameters' => array('context' => 'object')
                )
            )
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('address.name')
            ->add('type')
            ->add('developer', null, array())
        ;
    }

    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        if (!$childAdmin && !in_array($action, array('edit', 'show'))) {
            return;
        }

        $admin = $this->isChild() ? $this->getParent() : $this;
        $id = $admin->getRequest()->get('id');

        $menu->addChild('Показать объект', array('uri' => $admin->generateUrl('edit', array('id' => $id))));

        if ($this->isGranted('LIST')) {
            $menu->addChild('Building List', array(
                'uri' => $admin->generateUrl('admin.building.list', array('id' => $id))
            ));
        }
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('import', $this->getRouterIdParameter().'/import');
        $collection->add('import_mapping', $this->getRouterIdParameter().'/import-mapping');
    }
}
