<?php

namespace AppBundle\Admin;

use AppBundle\Entity\Street;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class StreetAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', 'text')
            ->add('type', 'sonata_type_model')
            ->add('district', 'sonata_type_model', array(
                'property' => 'name'
            ))
            ->add('subDistrict', 'sonata_type_model', array(
                'placeholder' => '-',
                'property' => 'name',
                'required' => false
            ))
        ;

        if ($this->isCurrentRoute('edit')) {
            $formMapper->add('district.city', 'sonata_type_model', array(
                'property' => 'name',
                'label' => 'Город'
            ));
        } elseif ($this->isCurrentRoute('create')) {

            $formMapper->add('district.city', 'sonata_type_model', array(
                'property' => 'name',
                'mapped' => false,
                'label' => 'Город'
            ));
        }
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->addIdentifier('type', 'text')
            ->add('district', null, array(
                'associated_property' => 'name')
            )
            ->add('SubDistrict', null, array(
                    'associated_property' => 'name')
            )
            ->add('district.city', null, array(
                    'associated_property' => 'name',
                    'label' => 'Город'
                )
            )
        ;
    }

    public function toString($object)
    {
        return $object instanceof Street
            ? $object->getName()
            : 'Street';
    }

    public function preUpdate($object)
    {
        // Привязка района к подрайону
        if ($object->getSubDistrict() !== null) {
            $object->getSubDistrict()->setDistrict($object->getDistrict());
        }
    }
}
