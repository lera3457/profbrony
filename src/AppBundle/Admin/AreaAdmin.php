<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;


class AreaAdmin extends AbstractAdmin
{

    public function configureRoutes(RouteCollection $collection)
    {
        $collection->clear();
        $collection->add('get', $this->getRouterIdParameter(). '/get' );
        $collection->add('remove', 'remove' );
        $collection->add('flat_add', $this->getRouterIdParameter(). '/flat/add' );
    }
}
