<?php

namespace AppBundle\Admin;

use AppBundle\Entity\Block;
use AppBundle\Entity\Floor;
use AppBundle\Utils\AggsHelper;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\HttpFoundation\RedirectResponse;

class FloorAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $em = $this->getConfigurationPool()->getContainer()->get('doctrine.orm.entity_manager');

        if ($blockId = $this->request->get('id')) {

            $floor = new Floor();

            $block = $em->getRepository(Block::class)->find($blockId);

            $last_floor_num = $em->getRepository(Floor::class)->findLastFloor($block);
            $floor_num = $last_floor_num ? ++$last_floor_num : 1;

            $floor->setNum($floor_num);
            $floor->setBlock($block);
            $em->persist($floor);
            $em->flush();

            // Пересчет агрегаций в доме
            $building = $floor->getBlock()->getBuilding();
            $aggsHelper = new AggsHelper();
            $aggsHelper->updateAggsDataInBuilding( $building, $em );

            $router = $this->getConfigurationPool()->getContainer()->get('router');
            $redirection = new RedirectResponse($router->generate('admin_app_building_board', array('id' => $building->getId())));
            $redirection->send();
        }

        return false;
    }

    public function preRemove($object)
    {
        $router = $this->getConfigurationPool()->getContainer()->get('router');
        $redirection = new RedirectResponse($router->generate('admin_app_building_board', array('id' => $object->getBlock()->getBuilding()->getId())));
        $redirection->send();
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
        ;
    }

    public function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('list');
        $collection->remove('delete');
        $collection->add('delete_floor', $this->getRouterIdParameter() . '/delete');
        $collection->add('add_layout', $this->getRouterIdParameter() . '/layout/{layoutId}/add');
        $collection->add('delete_layout', $this->getRouterIdParameter() . '/layout/delete');
    }

}
