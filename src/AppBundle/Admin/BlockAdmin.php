<?php

namespace AppBundle\Admin;

use AppBundle\Entity\Block;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\HttpFoundation\RedirectResponse;

class BlockAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name')
        ;
    }

    public function postUpdate($object)
    {
        $router = $this->getConfigurationPool()->getContainer()->get('router');
        $redirection = new RedirectResponse($router->generate('admin_app_building_board', array('id' => $object->getBuilding()->getId())));
        $redirection->send();
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
        ;
    }

    public function toString($object)
    {
        return $object instanceof Block
            ? $object->getName()
            : 'Block';
    }

    public function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('list');
        $collection->remove('delete');
        $collection->add('delete_block', $this->getRouterIdParameter() . '/delete');
    }
}
