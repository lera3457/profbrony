<?php

namespace AppBundle\Admin;
use AppBundle\Entity\Block;
use AppBundle\Entity\Building;
use AppBundle\Entity\Flat;
use AppBundle\Entity\Floor;
use AppBundle\Entity\FloorLayout;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class FloorLayoutAdmin extends AbstractAdmin
{
    protected $parentAssociationMapping = 'building';

    protected function configureFormFields(FormMapper $formMapper)
    {
        $floorLayout = $this->getSubject();
        $blocks = $floorLayout->getBuilding()->getBlocks();
        $block_choices = array();
        foreach ($blocks as $block){
            $block_choices[] = $block;
        }

        $formTitle = $this->getSubject()->getId() ? 'Редактировать планировку' : 'Создать планировку';
        $formMapper
            ->with($formTitle)
            ->add('block', 'sonata_type_model', array(
                'choices' => $block_choices,
                'btn_add' => false,
            ))
            ->add('image', 'sonata_type_model_list', array(), array(
                'link_parameters' => array(
                    'context' => 'floor_layout'
                )
            ))
            ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('block')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('image', null, array('template' => 'AppBundle:Admin:floor_layout_image.html.twig'))
            ->add('block')
            ->add('', null, array('template' => 'AppBundle:Admin:edit_area_field.html.twig'))
        ;
    }

    public function toString($object)
    {
        return $object instanceof FloorLayoutAdmin
            ? $object->getName()
            : 'Floor Layout';
    }

    public function configureRoutes(RouteCollection $collection)
    {
        $collection->add('areas', $this->getRouterIdParameter() . '/areas');
        $collection->add('floors', $this->getRouterIdParameter() . '/floors');
        $collection->add('area_create', $this->getRouterIdParameter() . '/area/create');
        $collection->add('area_list', $this->getRouterIdParameter() . '/area/list');
    }
}
