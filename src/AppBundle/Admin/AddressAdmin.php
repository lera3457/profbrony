<?php

namespace AppBundle\Admin;

use AppBundle\Entity\Address;
use AppBundle\Entity\Street;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class AddressAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('streetNum', 'text', array(
                'label' => 'Номер дома'
            ))
            ->add('street', 'sonata_type_model', array(
                'property' => 'name'
            ))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name', null, array(
            'label' => 'Номер дома'
        ));
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('streetNum', null, array(
                'label' => 'Номер дома'
            ))
            ->add('street', null, array(
                    'associated_property' => 'name')
            )
            ->add('street.district', null, array(
                    'associated_property' => 'name',
                    'label' => 'Район'
                )
            )
            ->add('street.district.subDistrict', null, array(
                    'associated_property' => 'name',
                    'label' => 'Микрорайон'
                )
            )
            ->add('street.district.city', null, array(
                    'associated_property' => 'name',
                    'label' => 'Город'
                )
            )
            ->add('street.district.city.region', null, array(
                    'associated_property' => 'name',
                    'label' => 'Регион'
                )
            )
        ;
    }

    public function toString($object)
    {
        return $object instanceof Address
            ? $object->getName()
            : 'Address';
    }


    public function prePersist($object)
    {
        // Формирование строки адреса
        $streetNum = $object->getStreetNum();
        $street = $object->getStreet();
        $streetType = $street->getType()->getNameAbbreviated();
        $district = $street->getDistrict();
        $city = $district->getCity();

        $streetName =  $city->getName() . ', ' . $streetType . '. ' .$street->getName() . ', ' . $streetNum;

        $object->setName($streetName);
    }
}
