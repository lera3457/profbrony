<?php

namespace AppBundle\Admin;

use AppBundle\Entity\Region;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\AdminInterface;

class RegionAdmin extends AbstractAdmin
{

//    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
//    {
//        if (!$childAdmin && !in_array($action, array('edit', 'show'))) {
//            return;
//        }
//
//        $admin = $this->isChild() ? $this->getParent() : $this;
//        $id = $admin->getRequest()->get('id');
//
//        if ($this->isGranted('LIST')) {
//            $menu->addChild('City List', array(
//                'uri' => $admin->generateUrl('admin.city.list', array('id' => $id))
//            ));
//        }
//    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Регион')
                ->add('name', 'text')
            ->end();
            //->add('cities', 'sonata_type_model',
            //    array(), array(
            //        'edit' => 'inline',
            //        'inline' => 'table',
            //        'sortable' => 'position',
            //    )
            //)
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name');
    }

    public function toString($object)
    {
        return $object instanceof Region
            ? $object->getName()
            : 'Region';
    }
}
