<?php

namespace AppBundle\Admin;

use AppBundle\Entity\City;
use AppBundle\Entity\Region;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class DistrictAdmin extends AbstractAdmin
{
//    protected $parentAssociationMapping = 'city';

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', 'text')
            ->add('city', 'sonata_type_model')
        ;

        if ($this->isCurrentRoute('edit')) {
            $formMapper->add('city.region', 'sonata_type_model');
        }
   }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('city', null, array(
                    'associated_property' => 'name')
            )
            ->add('city.region', null, array(
                    'associated_property' => 'name')
            )
        ;
    }

}
