<?php

namespace AppBundle\Admin;

use AppBundle\Entity\City;
use AppBundle\Entity\Region;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\AdminInterface;

class CityAdmin extends AbstractAdmin
{
//    protected $parentAssociationMapping = 'region';
//
//
//    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
//    {
//        if (!$childAdmin && !in_array($action, array('edit', 'show'))) {
//            return;
//        }
//
//        $admin = $this->isChild() ? $this->getParent() : $this;
//        $id = $admin->getRequest()->get('id');
//
//
//        if ($this->isGranted('LIST')) {
//            $menu->addChild('District List', array(
//                'uri' => $admin->generateUrl('admin.district.list', array('id' => $id))
//            ));
//
//        }
//    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', 'text')
            ->add('region', 'sonata_type_model', array(
                'class' => Region::class,
                'property' => 'name',
            ))
            //->add('districts', 'sonata_type_collection',
            //    array(), array(
            //        'edit' => 'inline',
            //        'inline' => 'table',
            //        'sortable' => 'position',
            //    ))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('region', null, array(
                    'associated_property' => 'name')
            )
        ;
    }

    public function toString($object)
    {
        return $object instanceof City
            ? $object->getName()
            : 'City';
    }
}
