<?php

namespace AppBundle\Admin;

use AppBundle\Entity\Building;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Admin\BreadcrumbsBuilder;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class BuildingAdmin extends AbstractAdmin
{
    protected $parentAssociationMapping = 'object';

    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        if (!$childAdmin && !in_array($action, array('edit', 'show'))) {
            return;
        }

        $admin = $this->isChild() ? $this->getParent() : $this;
        $id = $admin->getRequest()->get('id');
        $menu->addChild('Показать дом', array('uri' => $admin->generateUrl('edit', array('id' => $id))));
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formTitle = $this->getSubject()->getId() ? 'Редактировать дом' : 'Создать дом';
        $formMapper
            ->with($formTitle)
                ->add('name')
                ->add('dateStart', 'sonata_type_date_picker', array(
                    'dp_view_mode' => 'years',
                    'dp_min_view_mode' => 'months',
                    'format' => 'y/M'
                ))
                ->add('deadlineYear','sonata_type_date_picker', array(
                    'dp_view_mode' => 'years',
                    'dp_min_view_mode' => 'years',
                    'format' => 'y'
                ))
                ->add('deadlineQuarter', null, [
                    'attr' => ["class" => "your-custom-class"]
                ])
                ->add('isReady')
                ->add('fz_214')
                ->add('installment')
                ->add('mortgage')
                ->add('militaryMortgage')
                ->add('materialType')
                ->add('type')
                ->add('address', null, array(
                    'required' => true
                ))
                ->add('object')
            ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('object.name')
            ->add('address.name')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('address.name')
            ->add('object')
        ;
    }

    public function toString($object)
    {
        return $object instanceof Building
            ? $object->getName()
            : 'Building';
    }

    public function configureRoutes(RouteCollection $collection)
    {
        $collection->add('board', $this->getRouterIdParameter() . '/board');
    }

}
