<?php

namespace AppBundle\Admin;
use AppBundle\Entity\Block;
use AppBundle\Entity\Building;
use AppBundle\Entity\Flat;
use AppBundle\Entity\Floor;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class FlatAdmin extends AbstractAdmin
{

    protected function configureFormFields(FormMapper $formMapper)
    {
        if ($floorId = $this->request->get('id')) {
            $em = $this->getConfigurationPool()->getContainer()->get('doctrine.orm.entity_manager');
            $floor = $em->getRepository(Floor::class)->find($floorId);
            $flat = new Flat($floorId);
            $flat->setFloor($floor);
            $em->persist($flat);
            $em->flush();
            $router = $this->getConfigurationPool()->getContainer()->get('router');
            $redirection = new RedirectResponse($router->generate('admin_app_building_board', array('id' => $floor->getBlock()->getBuilding()->getId())));
            $redirection->send();
        }

        $formTitle = $this->getSubject()->getId() ? 'Редактировать квартиру' : 'Создать квартиру';
        $formMapper
            ->with($formTitle)
            ->add('num')
            ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('num')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('num')
        ;
    }

    public function toString($object)
    {
        return $object instanceof Flat
            ? $object->getName()
            : 'Flat';
    }

    public function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
        $collection->add('update_flat', $this->getRouterIdParameter() . '/update');
        $collection->add('delete_flat', $this->getRouterIdParameter() . '/delete');
    }
}
