<?php

namespace AppBundle\Admin;

use AppBundle\Entity\Developer;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\AdminInterface;

class DeveloperAdmin extends AbstractAdmin
{

    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        if (!$childAdmin && !in_array($action, array('edit', 'show'))) {
            return;
        }

        $admin = $this->isChild() ? $this->getParent() : $this;
        $id = $admin->getRequest()->get('id');

        //$menu->addChild('Показать застройщика', array('uri' => $admin->generateUrl('show', array('id' => $id))));

        //if ($this->isGranted('EDIT')) {
        //    $menu->addChild('Edit Playlist', array('uri' => $admin->generateUrl('edit', array('id' => $id))));
        //}
        $routes = $admin->getRoutes();
        if ($this->isGranted('LIST')) {
            $menu->addChild('Object List', array(
                'uri' => $admin->generateUrl('admin.object.list', array('id' => $id))
            ));
        }
    }


    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', 'text')
            ->add('address', 'sonata_type_model')
            //->add('objects', 'sonata_type_native_collection', array(
            //    //'type_options' => array(
            //    //    // Prevents the "Delete" option from being displayed
            //    //    'delete' => false,
            //    //    'delete_options' => array(
            //    //        // You may otherwise choose to put the field but hide it
            //    //        'type'         => 'hidden',
            //    //        // In that case, you need to fill in the options as well
            //    //        'type_options' => array(
            //    //            'mapped'   => false,
            //    //            'required' => false,
            //    //        )
            //    //    )
            //    //)
            //), array(
            //    'edit' => false,
            //    'inline' => 'table',
            //    'sortable' => 'position',
            //))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->addIdentifier('address');
    }

    public function toString($object)
    {
        return $object instanceof Developer
            ? $object->getName()
            : 'Developer';
    }
}
