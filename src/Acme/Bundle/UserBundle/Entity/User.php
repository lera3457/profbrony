<?php

namespace Acme\Bundle\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sonata\UserBundle\Entity\BaseUser;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * User
 *
 * @ORM\Table(name="fos_user_user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Developer", inversedBy="users")
     * @ORM\JoinColumn(name="developer_id", referencedColumnName="id")
     */
    protected $developer;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Agency", inversedBy="agency")
     * @ORM\JoinColumn(name="agency_id", referencedColumnName="id")
     */
    protected $agency;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set developer.
     *
     * @param mixed $developer
     *
     */
    public function setDeveloper($developer)
    {
        $this->developer = $developer;
    }

    /**
     * Get developer.
     *
     * @return mixed
     */
    public function getDeveloper()
    {
        return $this->developer;
    }

    /**
     * Set agency.
     *
     * @param mixed $agency
     *
     */
    public function setAgency($agency)
    {
        $this->agency = $agency;
    }

    /**
     * Get agency.
     *
     * @return mixed
     */
    public function getAgency()
    {
        return $this->agency;
    }


    public function setEmail($email)
    {

        // При регистрации пользователся создаем рандомный пароль
        if (!$this->password) {
            $password =  base64_encode( random_bytes(10) );
            $this->plainPassword = $password;
        }

        return parent::setEmail($email);
    }
}
