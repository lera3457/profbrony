<?php
/**
 * Created by PhpStorm.
 * User: che
 * Date: 19.03.18
 * Time: 14:31
 */

namespace Acme\Bundle\UserBundle\Admin;

use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\CoreBundle\Form\Type\DatePickerType;
use Sonata\UserBundle\Admin\Model\UserAdmin as SonataUserAdmin;
use Sonata\UserBundle\Form\Type\SecurityRolesType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\LocaleType;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class UserAdmin extends SonataUserAdmin
{
    protected $parentAssociationMapping = 'developer';

    protected $datagridValues = [

        // display the first page (default = 1)
        '_page' => 1,

        // reverse order (default = 'ASC')
        '_sort_order' => 'DESC',

        // name of the ordered field (default = the model's id field, if any)
        '_sort_by' => 'createdAt',
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {
//        parent::configureFormFields($formMapper);

       /* $formMapper
            ->tab('User')
                ->with('General')
                    ->add('developer', 'sonata_type_model', array(
                        'property' => 'name',
                        'label' => 'Застройщики'
                    ))
                ->end()
            ->end()
        ;*/

        // define group zoning
        $formMapper
            ->with('Profile', ['class' => 'col-md-6'])->end()
            ->with('General', ['class' => 'col-md-6'])->end()
            ->with('Status', ['class' => 'col-md-6'])->end()
            ->with('Groups', ['class' => 'col-md-6'])->end()
        ;

        $now = new \DateTime();

        $genderOptions = [
            'choices' => call_user_func([$this->getUserManager()->getClass(), 'getGenderList']),
            'required' => false,
            'translation_domain' => $this->getTranslationDomain(),
        ];

        // NEXT_MAJOR: Remove this when dropping support for SF 2.8
        if (method_exists(FormTypeInterface::class, 'setDefaultOptions')) {
            $genderOptions['choices_as_values'] = true;
        }

        $formMapper
            ->with('General')
            ->add('username')
            ->add('email')
            ->add('plainPassword', TextType::class, [
                'required' => (!$this->getSubject() || null === $this->getSubject()->getId()),
                'label' => 'Пароль'
            ])
            ->end()
            ->with('Profile')
            ->add('dateOfBirth', DatePickerType::class, [
                'years' => range(1900, $now->format('Y')),
                'dp_min_date' => '1-1-1900',
                'dp_max_date' => $now->format('c'),
                'required' => false,
                'format' => 'yyyy-MM-dd',
            ])
            ->add('firstname', null, ['required' => false])
            ->add('lastname', null, ['required' => false])
            ->add('phone', null, ['required' => false])
            ->add('developer', 'sonata_type_model', array(
                'property' => 'name',
                'label' => 'Застройщики',
                'required' => false,
            ))
            ->add('agency', 'sonata_type_model', array(
                'property' => 'name',
                'label' => 'Агенство',
                'required' => false,
            ))
            ->end()
            ->with('Status')
            ->add('enabled', null, ['required' => false])
            ->end()
            ->with('Groups')
            ->add('groups', ModelType::class, [
                'required' => false,
                'expanded' => true,
                'multiple' => true,
            ])
            ->end()
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
//        parent::configureListFields($listMapper);

        $listMapper
            ->addIdentifier('username')
            ->add('createdAt', null, array('format' => 'y-M-d'))
            ->add('groups', null, array('label' => 'Роль', 'sortable' => true))
            ->add('enabled', null, array('editable' => true, 'sortable' => false ))
            ->add('phone', null, array('label' => 'Телефон'))
            ->add('developer', null, array('label' => 'Застройщик', 'sortable' => true))
            ->add('agency', null, array('label' => 'Агенство', 'sortable' => true, 'header_style' => 'width: 15%'))
        ;
    }
}