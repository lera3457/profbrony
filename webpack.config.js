var Encore = require('@symfony/webpack-encore');

Encore
// directory where all compiled assets will be stored
    .setOutputPath('web/build/')

    // what's the public path to this directory (relative to your project's document root dir)
    .setPublicPath('/build')

    // empty the outputPath dir before each build
    .cleanupOutputBeforeBuild()


    .addEntry('main', './web/src/js/main.js')

    .addEntry('main_reg', './web/src/js/registration.js')

    .addEntry('ymap', './web/src/js/yandex-map/ymap.js')

    .addStyleEntry('style', './web/src/less/style.less')

    .addStyleEntry('admin', './web/src/less/admin.less')


    .createSharedEntry('vendor', [
        'jquery',
        'bootstrap',
        'admin-lte',
        'select2',
        'datatables',
        'datatables.net-bs',
        'datatables.net-responsive-bs',
        'jquery-slimscroll',
        'sweetalert2',

        'bootstrap/less/bootstrap.less',
        'admin-lte/dist/css/AdminLTE.css',
        'admin-lte/dist/css/skins/_all-skins.css',
        'font-awesome/less/font-awesome.less',
        'ionicons/dist/css/ionicons.css',
        'select2/dist/css/select2.css',
        'datatables.net-bs/css/dataTables.bootstrap.css',
        'datatables.net-responsive-bs/css/responsive.bootstrap.css',
        'sweetalert2/dist/sweetalert2.css'
    ])

    .enableLessLoader()

    // allow legacy applications to use $/jQuery as a global variable
    .autoProvidejQuery()

    .enableSourceMaps(!Encore.isProduction())

    // create hashed filenames (e.g. app.abc123.css)
    .enableVersioning()
;

// export the final configuration
module.exports = Encore.getWebpackConfig();