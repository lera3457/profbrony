<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170906094824 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE area (id INT AUTO_INCREMENT NOT NULL, layout_id INT DEFAULT NULL, points LONGTEXT NOT NULL, INDEX IDX_D7943D688C22AA1A (layout_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE building (id INT AUTO_INCREMENT NOT NULL, object_id INT NOT NULL, name VARCHAR(100) NOT NULL, slug VARCHAR(255) NOT NULL, INDEX IDX_E16F61D4232D562B (object_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE block (id INT AUTO_INCREMENT NOT NULL, building_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_831B97224D2A7E12 (building_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE developer (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE flat (id INT AUTO_INCREMENT NOT NULL, floor_id INT DEFAULT NULL, area_id INT DEFAULT NULL, num SMALLINT NOT NULL, INDEX IDX_554AAA44854679E2 (floor_id), INDEX IDX_554AAA44BD0F409C (area_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE flat_param (id INT AUTO_INCREMENT NOT NULL, type_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, required TINYINT(1) NOT NULL, show_in_board_form TINYINT(1) NOT NULL, INDEX IDX_1E420B5AC54C8C93 (type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE flat_param_type_value (id INT AUTO_INCREMENT NOT NULL, flat_param_type_id INT DEFAULT NULL, flat_param_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_8B945974EAA5549E (flat_param_type_id), INDEX IDX_8B9459747C4C973F (flat_param_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE floor (id INT AUTO_INCREMENT NOT NULL, block_id INT DEFAULT NULL, layout_id INT DEFAULT NULL, num SMALLINT NOT NULL, INDEX IDX_BE45D62EE9ED820C (block_id), INDEX IDX_BE45D62E8C22AA1A (layout_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE floor_layout (id INT AUTO_INCREMENT NOT NULL, block_id INT DEFAULT NULL, building_id INT DEFAULT NULL, updated_at DATETIME NOT NULL, image_name VARCHAR(255) DEFAULT NULL, image_original_name VARCHAR(255) DEFAULT NULL, image_mime_type VARCHAR(255) DEFAULT NULL, image_size INT DEFAULT NULL, INDEX IDX_F552DF02E9ED820C (block_id), INDEX IDX_F552DF024D2A7E12 (building_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE import_file (id INT AUTO_INCREMENT NOT NULL, object_id INT DEFAULT NULL, import_file_name VARCHAR(255) NOT NULL, import_file_size INT NOT NULL, updated_at DATETIME NOT NULL, mapping_array LONGTEXT DEFAULT NULL, INDEX IDX_61B3D890232D562B (object_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE object (id INT AUTO_INCREMENT NOT NULL, developer_id INT NOT NULL, type_id INT NOT NULL, name VARCHAR(100) NOT NULL, INDEX IDX_A8ADABEC64DD9267 (developer_id), INDEX IDX_A8ADABECC54C8C93 (type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE object_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE flat_param_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, is_editable TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE flat_param_value (id INT AUTO_INCREMENT NOT NULL, param_id INT NOT NULL, flat_id INT NOT NULL, value VARCHAR(255) DEFAULT NULL, INDEX IDX_6A2FB0E45647C863 (param_id), INDEX IDX_6A2FB0E4D3331C94 (flat_id), UNIQUE INDEX unique_idx (param_id, flat_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE fos_group (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', UNIQUE INDEX UNIQ_4B019DDB5E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE fos_user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(180) NOT NULL, username_canonical VARCHAR(180) NOT NULL, email VARCHAR(180) NOT NULL, email_canonical VARCHAR(180) NOT NULL, enabled TINYINT(1) NOT NULL, salt VARCHAR(255) DEFAULT NULL, password VARCHAR(255) NOT NULL, last_login DATETIME DEFAULT NULL, confirmation_token VARCHAR(180) DEFAULT NULL, password_requested_at DATETIME DEFAULT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', UNIQUE INDEX UNIQ_957A647992FC23A8 (username_canonical), UNIQUE INDEX UNIQ_957A6479A0D96FBF (email_canonical), UNIQUE INDEX UNIQ_957A6479C05FB297 (confirmation_token), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE fos_user_user_group (user_id INT NOT NULL, group_id INT NOT NULL, INDEX IDX_B3C77447A76ED395 (user_id), INDEX IDX_B3C77447FE54D947 (group_id), PRIMARY KEY(user_id, group_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE area ADD CONSTRAINT FK_D7943D688C22AA1A FOREIGN KEY (layout_id) REFERENCES floor_layout (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE building ADD CONSTRAINT FK_E16F61D4232D562B FOREIGN KEY (object_id) REFERENCES object (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE block ADD CONSTRAINT FK_831B97224D2A7E12 FOREIGN KEY (building_id) REFERENCES building (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE flat ADD CONSTRAINT FK_554AAA44854679E2 FOREIGN KEY (floor_id) REFERENCES floor (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE flat ADD CONSTRAINT FK_554AAA44BD0F409C FOREIGN KEY (area_id) REFERENCES area (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE flat_param ADD CONSTRAINT FK_1E420B5AC54C8C93 FOREIGN KEY (type_id) REFERENCES flat_param_type (id)');
        $this->addSql('ALTER TABLE flat_param_type_value ADD CONSTRAINT FK_8B945974EAA5549E FOREIGN KEY (flat_param_type_id) REFERENCES flat_param_type (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE flat_param_type_value ADD CONSTRAINT FK_8B9459747C4C973F FOREIGN KEY (flat_param_id) REFERENCES flat_param (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE floor ADD CONSTRAINT FK_BE45D62EE9ED820C FOREIGN KEY (block_id) REFERENCES block (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE floor ADD CONSTRAINT FK_BE45D62E8C22AA1A FOREIGN KEY (layout_id) REFERENCES floor_layout (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE floor_layout ADD CONSTRAINT FK_F552DF02E9ED820C FOREIGN KEY (block_id) REFERENCES block (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE floor_layout ADD CONSTRAINT FK_F552DF024D2A7E12 FOREIGN KEY (building_id) REFERENCES building (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE import_file ADD CONSTRAINT FK_61B3D890232D562B FOREIGN KEY (object_id) REFERENCES object (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE object ADD CONSTRAINT FK_A8ADABEC64DD9267 FOREIGN KEY (developer_id) REFERENCES developer (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE object ADD CONSTRAINT FK_A8ADABECC54C8C93 FOREIGN KEY (type_id) REFERENCES object_type (id)');
        $this->addSql('ALTER TABLE flat_param_value ADD CONSTRAINT FK_6A2FB0E45647C863 FOREIGN KEY (param_id) REFERENCES flat_param (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE flat_param_value ADD CONSTRAINT FK_6A2FB0E4D3331C94 FOREIGN KEY (flat_id) REFERENCES flat (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE fos_user_user_group ADD CONSTRAINT FK_B3C77447A76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE fos_user_user_group ADD CONSTRAINT FK_B3C77447FE54D947 FOREIGN KEY (group_id) REFERENCES fos_group (id)');
        $this->addSql('INSERT INTO `flat_param_type` (`id`, `name`, `type`, `is_editable`) VALUES
(1,	\'Строка\',	\'text\',	0),
(2,	\'Список (Много значений)\',	\'select\',	1),
(3,	\'Список (Одно значение)\',	\'checkbox\',	0),
(4,	\'Число\',	\'number\',	0);
INSERT INTO `flat_param` (`id`, `type_id`, `name`, `slug`, `required`, `show_in_board_form`) VALUES
(1,	4,	\'Номер квартиры\',	\'nomer-kvartiry\',	1,	1),
(2,	1,	\'Этаж\',	\'etazh\',	1,	1),
(3,	1,	\'Площадь, м2\',	\'ploshchad-m2\',	1,	1),
(4,	1,	\'Полная стоимость\',	\'polnaya-stoimost\',	1,	1),
(5,	2,	\'Количество комнат\',	\'kolichestvo-komnat\',	1,	1),
(6,	1,	\'Подъезд\',	\'podezd\',	1,	0),
(7,	1,	\'Дом\',	\'dom\',	0,	0),
(8,	3,	\'Студия\',	\'studiya\',	0,	1),
(9,	1,	\'Жилая\',	\'zhilaya\',	0,	1),
(10,	1,	\'Кухня\',	\'kuhnya\',	0,	1),
(12,	2,	\'Статус\',	\'status\',	0,	1),
(13,	4,	\'Цена за кв.м\',	\'cena-za-kv-m\',	0,	1),
(14,	1,	\'Площадь жилая\',	\'ploshchad-zhilaya\',	0,	1),
(15,	1,	\'Площадь санузлов\',	\'ploshchad-sanuzlov\',	0,	1),
(16,	1,	\'Кол-во балконов\',	\'kol-vo-balkonov\',	0,	1),
(17,	1,	\'Площадь балкона\',	\'ploshchad-balkona\',	0,	1),
(18,	1,	\'Кол-во лоджий\',	\'kol-vo-lodzhiy\',	0,	1),
(19,	1,	\'Площадь лоджии\',	\'ploshchad-lodzhii\',	0,	1),
(20,	3,	\'Акции на квартиру\',	\'akcii-na-kvartiru\',	0,	1);
INSERT INTO `flat_param_type_value` (`id`, `name`, `flat_param_type_id`, `flat_param_id`) VALUES
(1,	\'1\',	2,	5),
(2,	\'2\',	2,	5),
(3,	\'3\',	2,	5),
(4,	\'4\',	2,	5);
INSERT INTO `fos_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`) VALUES
(1,	\'admin\',	\'admin\',	\'admin@admin.com\',	\'admin@admin.com\',	1,	NULL,	\'$2y$13$Lgj4Lrf7sOKLyH9ncu0hzO3x1m9AyrMwkutUvFe4yg3Hh8U0iu2cK\',	\'2017-09-04 15:09:25\',	\'NULL\',	NULL,	\'a:1:{i:0;s:10:\"ROLE_ADMIN\";}\');
INSERT INTO `object_type` (`id`, `name`) VALUES
(1,	\'Жилой комплекс\'),
(2,	\'Район\');');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE flat DROP FOREIGN KEY FK_554AAA44BD0F409C');
        $this->addSql('ALTER TABLE block DROP FOREIGN KEY FK_831B97224D2A7E12');
        $this->addSql('ALTER TABLE floor_layout DROP FOREIGN KEY FK_F552DF024D2A7E12');
        $this->addSql('ALTER TABLE floor DROP FOREIGN KEY FK_BE45D62EE9ED820C');
        $this->addSql('ALTER TABLE floor_layout DROP FOREIGN KEY FK_F552DF02E9ED820C');
        $this->addSql('ALTER TABLE object DROP FOREIGN KEY FK_A8ADABEC64DD9267');
        $this->addSql('ALTER TABLE flat_param_value DROP FOREIGN KEY FK_6A2FB0E4D3331C94');
        $this->addSql('ALTER TABLE flat_param_type_value DROP FOREIGN KEY FK_8B9459747C4C973F');
        $this->addSql('ALTER TABLE flat_param_value DROP FOREIGN KEY FK_6A2FB0E45647C863');
        $this->addSql('ALTER TABLE flat DROP FOREIGN KEY FK_554AAA44854679E2');
        $this->addSql('ALTER TABLE area DROP FOREIGN KEY FK_D7943D688C22AA1A');
        $this->addSql('ALTER TABLE floor DROP FOREIGN KEY FK_BE45D62E8C22AA1A');
        $this->addSql('ALTER TABLE building DROP FOREIGN KEY FK_E16F61D4232D562B');
        $this->addSql('ALTER TABLE import_file DROP FOREIGN KEY FK_61B3D890232D562B');
        $this->addSql('ALTER TABLE object DROP FOREIGN KEY FK_A8ADABECC54C8C93');
        $this->addSql('ALTER TABLE flat_param DROP FOREIGN KEY FK_1E420B5AC54C8C93');
        $this->addSql('ALTER TABLE flat_param_type_value DROP FOREIGN KEY FK_8B945974EAA5549E');
        $this->addSql('ALTER TABLE fos_user_user_group DROP FOREIGN KEY FK_B3C77447FE54D947');
        $this->addSql('ALTER TABLE fos_user_user_group DROP FOREIGN KEY FK_B3C77447A76ED395');
        $this->addSql('DROP TABLE area');
        $this->addSql('DROP TABLE building');
        $this->addSql('DROP TABLE block');
        $this->addSql('DROP TABLE developer');
        $this->addSql('DROP TABLE flat');
        $this->addSql('DROP TABLE flat_param');
        $this->addSql('DROP TABLE flat_param_type_value');
        $this->addSql('DROP TABLE floor');
        $this->addSql('DROP TABLE floor_layout');
        $this->addSql('DROP TABLE import_file');
        $this->addSql('DROP TABLE object');
        $this->addSql('DROP TABLE object_type');
        $this->addSql('DROP TABLE flat_param_type');
        $this->addSql('DROP TABLE flat_param_value');
        $this->addSql('DROP TABLE fos_group');
        $this->addSql('DROP TABLE fos_user');
        $this->addSql('DROP TABLE fos_user_user_group');
    }
}
