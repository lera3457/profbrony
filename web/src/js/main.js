require('admin-lte');
require('select2');
require('icheck/icheck.js');
require('datatables');
require('datatables.net-bs');
require('datatables.net-responsive-bs');


require('font-awesome/less/font-awesome.less');
require('select2/dist/css/select2.min.css');
require('icheck/skins/all.css');
require('datatables.net-bs/css/dataTables.bootstrap.css');
require('datatables.net-responsive-bs/css/responsive.bootstrap.css');

var Building = require('./building');
new Building();

var SearchFilter = require('./search-filter');
new SearchFilter();

$.extend(true, $.fn.dataTable.defaults, {
    language: {
        "processing": "Подождите...",
        "search": "",
        "lengthMenu": "Показать _MENU_ записей",
        "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
        "infoEmpty": "Записи с 0 до 0 из 0 записей",
        "infoFiltered": "(отфильтровано из _MAX_ записей)",
        "infoPostFix": "",
        "loadingRecords": "Загрузка записей...",
        "zeroRecords": "Записи отсутствуют.",
        "emptyTable": "В таблице отсутствуют данные",
        "paginate": {
            "first": "Первая",
            "previous": "&laquo;",
            "next": "&raquo;",
            "last": "Последняя"
        },
        "aria": {
            "sortAscending": ": активировать для сортировки столбца по возрастанию",
            "sortDescending": ": активировать для сортировки столбца по убыванию"
        }
    }
});

$(function () {

    // Настройка datatables на странице результатов поиска
    var $searchResultTable = $('#search-result-table');
    $searchResultTable.dataTable({
        responsive: true,
        language: {
            searchPlaceholder: "Поиск",
            // lengthMenu: "Показать _MENU_",
            info: "Записи с _START_ до _END_ из _TOTAL_ записей",
            "lengthMenu":
                '<span class="hidden-xs">Показать </span>' +
                '<select class="form-control search-result-table__select2" style="width: 50px;">'+
                    '<option value="5">5</option>'+
                    '<option value="10">10</option>'+
                    '<option value="15">15</option>'+
                    '<option value="20">20</option>'+
                    '<option value="30">30</option>'+
                    // '<option value="-1">All</option>'+
                '</select>'+
                '<i class="fa fa-th-list visible-xs-inline-block text-muted search-result-table_length-icon"></i>'
        }
    });

    // Переход на карточку объекта с параметрами фильтра
    $searchResultTable.on('click', 'a.js-building-href', function(e) {
        e.preventDefault();
        var $el = $(e.currentTarget);
        window.location.href = $el.attr('href') + location.search;
    });

    $('#search-result-table-test').dataTable();
    $('.js-profile-table').dataTable();

    // функция включения и отключения дополнительной информации по комплексу в dataTables страницы результатов поиска
    // var buttonShowHide = ".js-view-desc";
    //     textShowHide   = ".js-view-text";
    // var textBtn;
    //     $(buttonShowHide).click(function(){
    //         if (textBtn === "off"){
    //             $(this).text("»");
    //             textBtn = "";
    //         }
    //         else {
    //             $(this).text("«");
    //             textBtn = "off";
    //         }
    //         $(textShowHide).toggle();
    //     });


    $('.filter-extended__select2').select2({
        placeholder: 'Не выбрано',
        'allowClear': true
    });

    $('.js-filter__item-deadline').select2({
        placeholder: 'Срок сдачи',
        'allowClear': true
    });

    $('.search-result-table__select2').select2({
        'allowClear': true
    });

    $('.filter__item-kolichestvo-komnat--select').select2({
        'allowClear': true,
        minimumResultsForSearch: -1,
        placeholder: function(){
            $(this).data('placeholder');
        }
    });

    $('.select2-type').select2({
        placeholder: "Выберите класс",
        allowClear: true
    });

    $('#collapse-filter').collapse({
        toggle: false
    });

    $('.filter-extended__checkbox').iCheck({
        checkboxClass: 'icheckbox_minimal-orange',
        radioClass: 'iradio_minimal-orange'
        // increaseArea: '20%' // optional
    });

    // c этим скриптом модальные окна работают корректно
    $('body').on('hidden.bs.modal', function () {
        if ($('.modal').hasClass('in')) {
            $('body').addClass('modal-open');
        }
    });

    // Botstrap tooltips delay
    $('.js-tooltips').tooltip({
        animation: true,
        delay: {show: 500, hide: 100}
    });



// Смена стилей фильтра при вызове расширенного меню

    $('#filter-extended')
        .on('show.bs.modal', function () {
            $('#filter')
                .addClass('active-filter')
                .removeClass('default-filter');
            // анимация стрелки
            // $(".js-filter__arrow").toggleClass('js-filter__arrow-flip');
            // добавляем нижний отступ фильтру при раскрытии выпадающего
            $('.filter').toggleClass('js-pad-b');
            $(".js-filter__btn-more").toggleClass('js-remove-btn-more');
        })
        .on('hide.bs.modal', function () {
            $('#filter')
                .addClass('default-filter')
                .removeClass('active-filter');
            // анимация стрелки
            // $(".js-filter__arrow").toggleClass('js-filter__arrow-flip');
            // добавляем нижний отступ фильтру при раскрытии выпадающего
            $('.filter').toggleClass('js-pad-b');
            $(".js-filter__btn-more").toggleClass('js-remove-btn-more');
        });
    // sidebar-toggle
    // $(".usersidebar-toggle").click(function(){
    //     $(".main-sidebar").toggle(200);
    //     $(".wrapper").toggleClass('content-margin-right');
    // });
    // if($(".main-sidebar").hidden())
    //     $(".wrapper").removeClass('content-margin-right');




    // кнопки чекбоксы в фильтре
    $('.filter__item-building-price--check').each(function () {
        var self = $(this),
            label = self.next(),
            label_text = label.text();

        label.remove();
        self.iCheck({
            checkboxClass: 'icheckbox_line',
            radioClass: 'iradio_line',
            insert: '<i class="fa fa-check"></i>' + label_text
        });
    });
    $('.filter__item-check-action').each(function () {
        var self = $(this),
            label = self.next(),
            label_text = label.text();

        label.remove();
        self.iCheck({
            checkboxClass: 'icheckbox_line',
            radioClass: 'iradio_line',
            insert: label_text
        });
    });


});
