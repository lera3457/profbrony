var building;
var Raphael = require('./layout_painter/raphael');
/**
 * Класс для обработки страницы домов
 *
 * @constructor
 */
var Building = function () {
    building = this;

    building.blockTabs = $('.js-block-tabs');
    building.blockBoard = $('.js-block-board');
    building.floorLayout = $('.js-floor-layout');

    building.canvas = $('#layout-canvas');
    building.canvasImage = building.canvas.find('img');
    building.paper = '';


    building.blockTabs.on('click', '.btn', function (e) {
        //var blockId = $(e.currentTarget).data('block-id');
        //building.getBlockInfo(blockId)
    });

    building.blockBoard.on('click', 'td:not(:first-child)', function (e) {
        var $cell = $(e.currentTarget);
        var flatId = $cell.data('flat-id');

        building.handleBoardCell($cell);
        building.handleFlat(flatId)
    });

    // Нальная загрузка, выводиться первая квартира слева сверху
    var firstFlat = building.blockBoard.find('tr:first-child td:nth-child(2)');
    building.handleFlat(firstFlat.data('flatId'));
    building.handleBoardCell(firstFlat);

};

$.extend(Building.prototype, {
    //getBlockInfo: function (blockId) {
    //    $.ajax({
    //        url: Routing.generate('get_block', {id: blockId}),
    //        method: "GET"
    //    }).then(function (data) {
    //        console.log(data);
    //    });
    //},

    /**
     * Обработчик выбора квартиры
     * @param flatId
     *
     */
    handleFlat: function (flatId) {

        $.ajax({
            url: Routing.generate('get_flat', {id: flatId}),
            method: "GET"
        }).then(

            /**
             * @param {object} data
             * @property {object} data.flat
             * @property flat.layoutImg
             * @property flat.points
             * @property flat.params
             */
            function (data) {

                if (data.flat.points !== "") {
                    building.createCanvas(
                        data.flat.layoutImg,
                        JSON.parse(data.flat.points)
                    );
                }

                building.showFlatParams(JSON.parse(data.flat.params));

        });
    },

    /**
     * Отображение параметров
     * @param params
     */
    showFlatParams: function (params) {

        var className;
        // Отображение всех параметров квартиры
        $.each(params, function(k, val) {
            className = '.js-' + k.replace('_', '-');
            val = val !== null ? val : '-';
            val = $.isNumeric(val) ? val.toLocaleString('ru-RU') : val;
            $(className).text(val);
        });

        // Лэйбл "Акция"
        if (params.action) {
            $('.js-action-label').html('<a class="btn label label-danger box-title" href="#">Акция!</a>')
        } else {
            $('.js-action-label').empty();
        }
    },
    handleBoardCell: function ($cell) {

        building.blockBoard.find('.js-flat-checked').removeClass('building-board__item-checked');
        if (!$cell.find('.js-flat-checked').hasClass('building-board__item-checked')) {
            $cell.find('.js-flat-checked').addClass('building-board__item-checked');
        } else {
            $cell.find('.js-flat-checked').removeClass('building-board__item-checked');
        }

    },
    createCanvas: function (layoutImage, points) {

        var img = new Image();

        img.src = layoutImage;
        img.addEventListener("load", function(){

            var $wrapper = $(building.floorLayout.find('.box-body'));
            var wrapperWidth = $wrapper.width();
            var imgWidth = this.naturalWidth;
            // Коэффициент разницы размеров между оберктой и изображением. Нужен для заполнения обертки на 100%
            var sizeCoefficient = wrapperWidth / imgWidth;
            var width = wrapperWidth;
            var height = sizeCoefficient * this.naturalHeight;


            building.canvas.empty();

            building.paper = Raphael(
                building.canvas.attr('id'),
                width,
                height

            );

            building.canvas.css({
                width: width,
                height: height,
                background: 'url(' +  layoutImage + ')'
            });

            building.drawArea(points, sizeCoefficient)

        });
    },

    /**
     * Отображение зон планировок по координатам
     * @param points
     * @param sizeCoefficient коэффициент разницы в размерах между оберткой и изображением.
     */
    drawArea: function (points, sizeCoefficient) {

        var path = '';
        var x = 0;
        var y = 0;
        for (var i in points) {

            path += (path === '' ? 'M' : 'L');
            if (points.hasOwnProperty(i)) {
                x = sizeCoefficient ? (sizeCoefficient * points[i].x) : points[i].x;
                y = sizeCoefficient ? (sizeCoefficient * points[i].y) : points[i].y;
                path += x + ' ' + y;
            }
        }

        building.paper
            .path(path)
            .attr({
                'fill': '#444',
                'fill-opacity': 0.5,
                'stroke-width': 2
            });

    }
});

function showLoader() {
    var html = '<div id="loader"><div class="loader-wrapper"><div id="inTurnFadingTextG"><div id="inTurnFadingTextG_1" class="inTurnFadingTextG">П</div><div id="inTurnFadingTextG_2" class="inTurnFadingTextG">р</div><div id="inTurnFadingTextG_3" class="inTurnFadingTextG">о</div><div id="inTurnFadingTextG_4" class="inTurnFadingTextG">ф</div><div id="inTurnFadingTextG_5" class="inTurnFadingTextG">Б</div><div id="inTurnFadingTextG_6" class="inTurnFadingTextG">р</div><div id="inTurnFadingTextG_7" class="inTurnFadingTextG">о</div><div id="inTurnFadingTextG_8" class="inTurnFadingTextG">н</div><div id="inTurnFadingTextG_9" class="inTurnFadingTextG">ь</div></div></div></div>';
    $('body').append(html);
    return false;
}


function hideLoader() {
    $('#loader').remove();
    return false;
}

module.exports = Building;