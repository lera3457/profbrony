var searchFilter;

var SearchFilter = function () {
    searchFilter = this;
    searchFilter.roomParamName = 'rC';
    searchFilter.deadlineParamName = 'deadline';
    searchFilter.districtParamName = 'district';
    searchFilter.subDistrictParamName = 'subDistrict';
    searchFilter.decorParamName = 'decor';
    searchFilter.materialParamName = 'material';
    searchFilter.typeParamName = 'type';
    searchFilter.$form = $('.js-search-filter');

    searchFilter.$form.on('submit', function (e) {
        searchFilter._handleForm(e);
    });

};

$.extend(SearchFilter.prototype, {
    _handleForm: function (e) {
        e.preventDefault();

        var $form = $(e.currentTarget).serializeArray();
        var params = searchFilter._getParams($form);

        params[searchFilter.roomParamName] = searchFilter._getArrayParams($form, searchFilter.roomParamName);
        params[searchFilter.deadlineParamName] = searchFilter._getArrayParams($form, searchFilter.deadlineParamName);
        params[searchFilter.districtParamName] = searchFilter._getArrayParams($form, searchFilter.districtParamName);
        params[searchFilter.subDistrictParamName] = searchFilter._getArrayParams($form, searchFilter.subDistrictParamName);
        params[searchFilter.decorParamName] = searchFilter._getArrayParams($form, searchFilter.decorParamName);
        params[searchFilter.materialParamName] = searchFilter._getArrayParams($form, searchFilter.materialParamName);
        params[searchFilter.typeParamName] = searchFilter._getArrayParams($form, searchFilter.typeParamName);
        params = searchFilter._convertParamsToString(params);

        window.location = Routing.generate('search_result') + '?' + params;

        //window.location = location.pathname.search('building')
            //    ? location.pathname + '?' + params
            //    : Routing.generate('search_result') + '?' + params
    },
    _convertParamsToString: function (params) {
        return Object.keys(params)
            .map(function (key) {

                if (params[key].length > 0) {
                    return key + '=' + params[key] + '&';
                }
            })
            .join('').slice(0, -1);

    },
    _getParams: function ($form) {
        var formData = [];
        $.each($form, function (key, fieldData) {
            if (fieldData.name.indexOf('[]') === -1) {

                formData[fieldData.name] = fieldData.value
            }

        });
        return formData;
    },
    /**
     * Преобразование массива параметров в строку
     * @param $form
     * @param paramName
     * @returns {string}
     * @private
     */
    _getArrayParams: function ($form, paramName) {

        var params = '';

        $.each($form, function (key, fieldData) {

            if (fieldData.name === paramName + '[]') {
                params += fieldData.value + ',';
            }
        });
        if (params.length > 0) {
            params = params.slice(0, -1);
        }

        return params;
    }
});

module.exports = SearchFilter;