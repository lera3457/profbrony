var painter;
//var Raphael = new Raphael;

/**
 * Класс формирования шахматки
 * @constructor
 */

var Board = function () {
    var self = this;

    this.$appForm = $('.js-floor-layout-app-form');
    this.$flatsSelector = $('.js-add-floor-to-layout');
    this.$table = $('.js-block-board');

    this.layoutId = this.$appForm.data('floor-layout-id');
    this.blockId = this.$appForm.data('block-id');

    this.initFlats();

    this.$flatsSelector
        .on('change', function (e) {
            if (e.added) {
                self.addFloor(e.added.id);
            }

            if (e.removed) {
                self.removeFloor(e.removed.id);
            }
        });

    this.$table
        .on('click', 'td', function (e) {
            var $td = $(e.currentTarget);
            var $tr = $td.closest('tr');

            if ($tr.hasClass('selected')) {

                if (!$td.hasClass('checked') && !$td.hasClass('js-floor-num')) {
                    $td.addClass('checked');
                } else {
                    $td.removeClass('checked');
                }
            }
        })
};

$.extend(Board.prototype, {
    initFlats: function () {
        var self = this;
        $.ajax({
            url: Routing.generate("admin_app_floorlayout_floors", {id: this.layoutId}),
            method: "GET"
        }).then(function (data) {

            // Удаляется select2 созданный по умолчанию
            self.$flatsSelector.select2('destroy');
            self.$flatsSelector.splice(0, 1);

            $.each(data, function (k, v) {
                var $option = $('<option></option>');
                if (v.floor.selected === true) {
                    $option.attr('selected', 'selected');
                }
                if (v.floor.active === true) {
                    self.$flatsSelector.append(
                        $option.text(v.floor.num).val(v.floor.id)
                    );
                }
            });

            self.$appForm.show();
            self.$flatsSelector.select2({
                    width: '100%'
            });
            self.generateBoard(data);
            hideLoader();
        });
    },
    addFloor: function (floorId) {

        var self = this;

        showLoader();

        $.ajax({
            url: Routing.generate('admin_app_floor_add_layout', {layoutId: self.layoutId, id: floorId}),
            method: 'POST'
        }).then(function (data) {
            var $row = self.$table.find('tr[data-floor-id="' + floorId + '"]');
            $row.addClass('selected');

            hideLoader();
        });
    },
    removeFloor: function (floorId) {
        var self = this;
        showLoader();

        $.ajax({
            url: Routing.generate('admin_app_floor_delete_layout', {id: floorId}),
            method: 'DELETE'
        }).then(function (data) {

            var $row = self.$table.find('tr[data-floor-id="' + floorId + '"]');
            $row.removeClass('selected');

            hideLoader();
        });
    },
    addFlats: function (floor) {
        var $table = $('.js-block-board');
        var $rows = $table.find('tr');
        var $tr = '';
        var $td = '';
        if ($rows.length === 0) {
            $tr = $('<tr></tr>');
            $td = $('<td class="js-floor-num"></td>').text(floor.num);
            $tr.append($td);
            $tr.attr('data-floor-id', floor.id);
            $.each(floor.flats, function (k, v) {
                $td = $('<td></td>');
                $td.attr('data-flat-id', v.id);
                $td.text(v.num);
                $tr.append($td);
            });
            $table.append($tr);
        } else {
            var prevFloor = 0;
            var curFloor = 0;
            $.each($rows, function (k, v) {

                var $curRow = $(v);
                curFloor = $curRow.find('td:first-child').text();
                $tr = $('<tr></tr>');
                $td = $('<td class="js-floor-num"></td>').text(floor.num);
                $tr.append($td);
                $tr.attr('data-floor-id', floor.id);

                $.each(floor.flats, function (k, v) {
                    $td = $('<td></td>');
                    $td.attr('data-flat-id', v.id);
                    $td.text(v.num);
                    $tr.append($td);
                });

                if ((prevFloor < floor.num) && (+floor.num < +curFloor)) {
                    $curRow.after($tr);
                }
                prevFloor = curFloor;
            });
        }
    },
    removeFlats: function (floor) {

    },
    generateBoard: function (block) {
        var self = this;
        var $tr = '';
        var $td = '';
        self.$table.empty();

        $.each(block, function (k, v) {
            $tr = $('<tr></tr>');
            $td = $('<td class="js-floor-num"></td>').html('<span>' + v.floor.num + '</span>');
            $tr.append($td).attr('data-floor-id', v.floor.id);

            if (v.floor.active === true) $tr.addClass('active');
            if (v.floor.selected) $tr.addClass('selected');

            $.each(v.floor.flats, function (k, v) {
                $td = $('<td></td>').attr('data-flat-id', v).html('<span>' + k + '</span>');
                $tr.append($td);
            });
            self.$table.append($tr);
        });
    },
    addArea: function (area) {
        var areaId = area.markId;
        var $table = $('.js-block-board');
        var $flats = $table.find('td.checked');
        var flatsArray = [];
        var flatId;

        $('#painter-context-menu-layer').hide();

        $.each($flats, function (k, v) {
            flatId = $(v).data('flat-id');

            flatsArray.push(flatId);
        });

        if (flatsArray.length > 0) {
            //showLoader();
            $.ajax({
                url: Routing.generate('admin_app_area_flat_add', {id: areaId}),
                method: "POST",
                data: {flats: flatsArray}
            }).then(function (data) {
                hideLoader();
            });
        }
    },
    addAreaFlats: function (area) {
        var areaId = area.markId;
        $.ajax({
            url: Routing.generate('admin_app_area_get', {id: areaId}),
            method: "GET"
        }).then(function (data) {
            var $table = $('.js-block-board');
            $.each(data.flats, function (k, v) {
                var $td = $table.find('td[data-flat-id="'+ v.id +'"]');
                if ($td.length > 0) {
                    $td.addClass('checked');
                }
            });
        });
        return true;
    },
    clearAreaFlats: function() {
        var $flats = $('.js-block-board').find('td.checked');
        $.each ($flats, function (k, v) {
            $(v).removeClass('checked');
        });
    }
});


/**
 * Класс редактора разметки
 *
 * @param canvas - jQuery-объект для канвы
 * @param options - опции
 * @constructor
 */
var Painter = function (canvas, options) {
    painter = this;
    this.isShift = false;

    painter.MODE_IDLE = 'Idle';
    painter.MODE_PAINT = 'Paint';
    painter.MODE_MENU = 'Menu';

    painter.TYPE_TOOL = 'Tool';
    painter.TYPE_LINE = 'Line';
    painter.TYPE_POINT = 'Point';
    painter.TYPE_AREA = 'Area';
    painter.TYPE_LABEL = 'Label';

    painter.nonTargetTypes = [
        painter.TYPE_TOOL,
        painter.TYPE_LINE,
        painter.TYPE_LABEL
    ];

    painter.BUTTON_LEFT = 1;
    painter.BUTTON_RIGHT = 3;

    painter.canvas = canvas;
    painter.paper = Raphael(canvas.attr('id'), canvas.width(), canvas.height());
    painter.toolLine = painter.drawToolLine();

    painter.options = $.extend({
        functionContextMenu: null,
        functionContextMenuHide: null
    }, options);

    painter.listAreaUrl = canvas.data('list-area-url');
    painter.saveAreaUrl = canvas.data('save-area-url');
    painter.deleteAreaUrl = canvas.data('delete-area-url');
    painter.relateAreaUrl = canvas.data('relate-area-url');
    painter.unrelateAreaUrl = canvas.data('unrelate-area-url');


    painter.roleIsGranted = {
        //create: canvas.data('role-create'),
        //edit:   canvas.data('role-edit'),
        //delete: canvas.data('role-delete')
        create: true,
        edit: true,
        delete: true
    };

    painter.mode = painter.MODE_IDLE;
    painter.currentPath = [];
    painter.tempLines = [];
    painter.areas = [];
    painter.activeArea = null;

    //Загрузка сохраненных областей
    painter.initAreas();

    painter.bindEvents();

    painter.initBoard();

    document.addEventListener('keydown', function (event) {
        if (event.shiftKey) {
            painter.isShift = true;
        }
    });

    document.addEventListener('keyup', function (event) {
        if (event.keyCode === 16) {
            painter.isShift = false;
        }
    });

    $('.js-painter-context-menu-delete').click(function (e) {
        e.preventDefault();
        painter.deleteActiveArea();
        painter.executeContextMenuHide();
    });
    $('.js-painter-context-menu-relate').click(function (e) {
        e.preventDefault();
        Board.prototype.addArea(painter.activeArea);

    });

};

$.extend(Painter.prototype, {
    /**
     * Получение координат мыши относительно канвы
     *
     * @param e - событие клика
     * @returns {{x: number, y: number}}
     */
    getCoordinates: function (e) {
        return {
            x: e.pageX - 1 - this.canvas.offset().left,
            y: e.pageY - 1 - this.canvas.offset().top
        };
    },
    /**
     * Загрузка сохраненных областей
     */
    initAreas: function () {
        var $this = this;

        showLoader();
        $.getJSON(this.listAreaUrl, function (response) {
            if (response.success) {
                $this.drawAreas(response.areas);
            }
        });
    },
    initBoard: function () {
        new Board();
    },
    /**
     * Отрисовка сохраненных областей
     */
    drawAreas: function (areas) {
        for (var a in areas) {
            for (var p in areas[a].points) {
                this.addPoint({
                    x: areas[a].points[p].x,
                    y: areas[a].points[p].y
                });
            }

            this.closePath(areas[a].id, areas[a].preset);
        }
    },
    /**
     * Назначение событий канвы
     */
    bindEvents: function () {
        var $this = this;
        this.canvas
            .contextmenu(function (e) {
                e.preventDefault()
            })
            .mouseup(function (e) {

                var method = 'click' + e.which;
                var target = false;
                var coordinates = $this.getCoordinates(e);

                var clickedElements = $this.paper.getElementsByPoint(coordinates.x, coordinates.y);

                clickedElements.forEach(function (el) {
                    if ($this.nonTargetTypes.indexOf(el.markType) < 0) {
                        target = el;
                    }
                });

                if (typeof($this[method]) === 'function') {
                    $this[method].call($this, coordinates, target);
                }
            })
            .mousemove(function (e) {
                if ($this.mode === $this.MODE_PAINT) {
                    $this.reDrawToolLine(e);
                }
            });
    },
    /**
     * Создание вспомогательной линии
     *
     * @returns {Line}
     */
    drawToolLine: function () {
        var toolLine = new Line();

        toolLine.el.markType = this.TYPE_TOOL;

        return toolLine;
    },
    /**
     * Перерисовка вспомогательной линии при движении мышью
     *
     * @param coordinates - координаты мыши относительно канвы или событие клика
     */
    reDrawToolLine: function (coordinates) {
        if (typeof(coordinates.x) === 'undefined' && typeof(coordinates.y) === 'undefined') {
            coordinates = this.getCoordinates(coordinates);
        }

        coordinates = this.shiftCoordinatesTransform(coordinates);

        this.toolLine.reDraw(this.currentPath[this.currentPath.length - 1], coordinates);
    },
    /**
     * Левый клик
     *
     * @param coordinates - координаты курсора относительно канвы
     * @param target - цель клика
     */
    click1: function (coordinates, target) {
        var method = 'click1Mode' + this.mode + (target ? target.markType : '');
        if (typeof(this[method]) === 'function') {
            this[method].call(this, coordinates, target);
        }
    },
    /**
     * Левый клик в режиме простоя по пустому месту
     *
     * @param coordinates - координаты курсора относительно канвы
     */
    click1ModeIdle: function (coordinates) {
        if (painter.roleIsGranted.create) {
            this.addPoint(coordinates);

            this.mode = this.MODE_PAINT;
        }
    },
    /**
     * Левый клик в режиме простоя по узлу
     *
     * @param coordinates - координаты курсора относительно канвы
     * @param point - узел
     */
    click1ModeIdlePoint: function (coordinates, point) {
        if (painter.roleIsGranted.create) {
            this.addPoint({
                x: point.attrs.cx,
                y: point.attrs.cy
            });

            this.mode = this.MODE_PAINT;
        }
    },
    /**
     * Левый клик в режиме простоя по области
     */
    click1ModeIdleArea: function (coordinates, area) {
        this.click3ModeIdleArea(coordinates, area);
    },
    /**
     * Левый клик в режиме рисования по пустому месту
     *
     * @param coordinates - координаты курсора относительно канвы
     */
    click1ModePaint: function (coordinates) {

        coordinates = this.shiftCoordinatesTransform(coordinates);

        this.addPoint(coordinates);

        this.drawLine();
    },
    /**
     * Левый клик в режиме рисования по узлу
     *
     * @param coordinates - координаты курсора относительно канвы
     * @param point - узел
     */
    click1ModePaintPoint: function (coordinates, point) {
        if (this.currentPath[0].el === point) {
            this.closePath();
        } else if (!this.inCurrentPath(point)) {
            this.click1ModePaint({
                x: point.attrs.cx,
                y: point.attrs.cy
            });
        }
    },
    /**
     * Левый клик в режиме рисования по области
     *
     * @param coordinates - координаты курсора относительно канвы
     */
    click1ModePaintArea: function (coordinates) {
        this.click1ModePaint(coordinates);
    },

    /**
     * Правый клик
     *
     * @param coordinates - координаты курсора относительно канвы
     * @param target - цель клика
     */
    click3: function (coordinates, target) {
        var method = 'click3Mode' + this.mode + (target && this.mode === this.MODE_IDLE ? target.markType : '');

        if (typeof(this[method]) === 'function') {
            this[method].call(this, coordinates, target);
        }
    },
    /**
     * Правый клик по области в режиме простоя
     *
     * @param coordinates - координаты курсора относительно канвы
     * @param area - область
     */
    click3ModeIdleArea: function (coordinates, area) {
        if (painter.roleIsGranted.edit || painter.roleIsGranted.delete) {
            this.executeContextMenu(coordinates, area);
        }

    },
    /**
     * Правый клик в режиме рисования
     *
     * @param coordinates - координаты курсора относительно канвы
     */
    click3ModePaint: function (coordinates) {
        if (painter.currentPath.length) {
            painter.currentPath.pop().el.remove();
        }

        if (painter.tempLines.length) {
            painter.tempLines.pop().el.remove();
        }

        if (painter.currentPath.length) {
            painter.reDrawToolLine(coordinates);
        } else {
            this.cancelDrawing();
        }
    },
    /**
     * Левый клик в режиме меню
     */
    click1ModeMenu: function () {
        this.executeContextMenuHide();
    },
    /**
     * Левый клик в режиме меню
     */
    painterclick1ModeMenuArea: function (coordinates, area) {
        this.click3ModeMenu(coordinates, area);
    },
    /**
     * Правый клик в режиме меню
     */
    click3ModeMenu: function (coordinates, target) {
        if (target.markType === this.TYPE_AREA) {
            this.click3ModeIdleArea(coordinates, target);
        } else {
            this.click1ModeMenu(coordinates, target);
        }
    },
    /**
     * Вызов функции "Контекстное меню"
     *
     * @param coordinates - координаты курсора относительно канвы
     * @param area - область
     */
    executeContextMenu: function (coordinates, area) {
        if (typeof(this.options.functionContextMenu) === 'function') {
            this.options.functionContextMenu(coordinates, area);

            this.activeArea = area;

            this.mode = this.MODE_MENU;

            Board.prototype.addAreaFlats(area);
        }
    },
    /**
     * Вызов функции "Скрытие контекстного меню"
     */
    executeContextMenuHide: function () {
        if (typeof(this.options.functionContextMenuHide) === 'function') {
            this.options.functionContextMenuHide();

            this.activeArea = null;

            this.mode = this.MODE_IDLE;
            Board.prototype.clearAreaFlats();
        }
    },
    /**
     * Узел является частью текущего пути
     *
     * @param point - узел
     * @returns {boolean}
     */
    inCurrentPath: function (point) {
        for (var i in this.currentPath) {
            if (this.currentPath[i].el === point) {
                return true;
            }
        }

        return false;
    },
    /**
     * Добавление узла
     *
     * @param coordinates - координаты курсора относительно канвы
     */
    addPoint: function (coordinates) {
        var isFirst = (!this.currentPath.length);
        if (this.currentPath.length > 1) {
            this.currentPath[this.currentPath.length - 1].setDefault();
        }

        this.currentPath.push(new Point(coordinates, isFirst));
    },
    /**
     * Соединение двух последних узлов линией
     */
    drawLine: function () {
        this.tempLines.push(new Line(
            this.currentPath[painter.currentPath.length - 2],
            this.currentPath[painter.currentPath.length - 1]
        ));
    },
    /**
     * Завершение рисования пути
     *
     * @param areaId - ID области, если она отрисовывается при инициализации
     * @param preset - пресет, если область связана
     */
    closePath: function (areaId, preset) {
        var area = new Area(this.currentPath, areaId);

        if (preset) {
            area.setRelated(preset.code);
        }

        this.addArea(area);

        this.currentPath[0].setDefault();
        this.currentPath[this.currentPath.length - 1].setDefault();

        this.cancelDrawing();
    },
    /**
     * Добавление области к разметке
     *
     * @param area - область
     */
    addArea: function (area) {
        var $this = this;
        if (area.id) {
            this.areas.push(area);
        } else {
            showLoader();
            $.post(painter.saveAreaUrl, {points: area.serialize()}, function (response) {
                hideLoader();

                if (response.success) {
                    area.setId(response.area.id);

                    $this.areas.push(area);
                } else {
                    area.remove();
                }
            });
        }
    },
    /**
     * Отмена режима рисования и переход в режим простоя
     */
    cancelDrawing: function () {
        for (var i in this.tempLines) {
            this.tempLines[i].el.remove();
        }

        this.toolLine.reDraw();

        this.mode = this.MODE_IDLE;
        this.currentPath = [];
        this.tempLines = [];
    },
    getArea: function (markId, remove) {
        for (var i in this.areas) {
            if (this.areas[i] && this.areas[i].id === markId) {
                var area = this.areas[i];

                remove && delete this.areas[i];

                return area;
            }
        }

        return null;
    },
    /**
     * Удаляет выделенную область
     */
    deleteActiveArea: function () {
        if (this.activeArea !== null) {
            var area = this.getArea(this.activeArea.markId, true);

            if (area !== null) {
                showLoader();
                $.ajax({
                    url: painter.deleteAreaUrl,
                    method: 'POST',
                    data: {id: area.id}
                }).then(function (response) {
                    hideLoader();
                    if (response.success) {
                        area.remove();
                    }
                });
            }

            this.activeArea = null;
        }
    },
    /**
     * Связывает выделенную область с пресетом
     *
     * @param presetId - ID пресета
     */
    relateActiveArea: function (presetId) {
        console.log('Hello');
        if (this.activeArea !== null) {
            var area = this.getArea(this.activeArea.markId);

            if (area !== null) {
                showLoader();

                $.post(painter.relateAreaUrl, {areaId: area.id, presetId: presetId}, function (response) {
                    hideLoader();

                    if (response.success) {
                        if (typeof response.preset.code !== 'undefined') {
                            area.setRelated(response.preset.code);
                        }

                    }
                });
            }
        }
    },
    /**
     * Отвязывает область от пресета
     */
    unrelateActiveArea: function () {
        if (this.activeArea !== null) {
            var area = this.getArea(this.activeArea.markId);

            if (area !== null) {
                showLoader();

                $.post(painter.unrelateAreaUrl, {id: area.id}, function (response) {
                    hideLoader();

                    if (response.success) {
                        area.setUnrelated();
                    }
                });
            }
        }
    },
    shiftCoordinatesTransform: function (coordinates) {
        var x1 = this.currentPath[this.currentPath.length - 1].x,
            x2 = coordinates.x,
            y1 = this.currentPath[this.currentPath.length - 1].y,
            y2 = coordinates.y,
            angle = 0;


        //// При зажатом шифте рисуем прямые линии
        if (this.isShift) {
            angle = Math.atan((y2 - y1) / (x2 - x1)) * (180 / Math.PI);
            if (angle >= 0 && angle <= 90) {
                if (angle > 45) {
                    coordinates.x = x1;
                } else {
                    coordinates.y = y1;
                }
            } else {
                if (angle < -45) {
                    coordinates.x = x1;
                } else {
                    coordinates.y = y1;
                }
            }
        }

        return coordinates;
    }

});


/**
 * Класс линии
 *
 * @param start - координаты начала линии относительно канвы
 * @param end - координаты конца линии относительно канвы
 * @constructor
 */
var Line = function (start, end) {
    this.STROKE = 2;

    this.setCoordinates(start, end);

    this.el = this.draw();

    this.el.markType = painter.TYPE_LINE;
};

$.extend(Line.prototype, {
    /**
     * Сохранение координат линии
     *
     * @param start - координаты начала линии относительно канвы
     * @param end - координаты конца линии относительно канвы
     */
    setCoordinates: function (start, end) {
        this.x1 = (start ? start.x || 0 : 0);
        this.y1 = (start ? start.y || 0 : 0);
        this.x2 = (end ? end.x || 0 : 0);
        this.y2 = (end ? end.y || 0 : 0);
    },
    /**
     * Отрисовка линии
     *
     * @returns Raphael Object
     */
    draw: function () {
        return painter.paper
            .path(this.getPath())
            .attr({
                'stroke-width': this.STROKE
            })
            .toBack();
    },
    /**
     * Перерисовка линии
     *
     * @param start - координаты начала линии относительно канвы
     * @param end - координаты конца линии относительно канвы
     * @returns Raphael Object
     */
    reDraw: function (start, end) {
        this.setCoordinates(start, end);

        return this.el.attr('path', this.getPath());
    },
    /**
     * Формирование описания пути из координат линии
     *
     * @returns {string}
     */
    getPath: function () {
        return 'M' + this.x1 + ' ' + this.y1 + 'L' + this.x2 + ' ' + this.y2;
    }
});


/**
 * Класс узла
 *
 * @param coordinates - координаты мыши относительно канвы
 * @constructor
 */
var Point = function (coordinates, isFirst) {
    this.RADIUS = 6;
    this.COLOR = (isFirst ? '#0B0' : '#45D4FF');
    this.DEFAULT = '#FFF';
    this.ACTIVE = '#9CF';
    this.STROKE = 2;

    this.x = coordinates.x;
    this.y = coordinates.y;

    this.el = this.draw();

    this.el.markType = painter.TYPE_POINT;
};
$.extend(Point.prototype, {
    /**
     * Отрисовка узла
     *
     * @returns Raphael Object
     */
    draw: function () {
        var $this = this;

        return painter.paper
            .circle(this.x, this.y, this.RADIUS)
            .attr({
                'fill': this.COLOR,
                'stroke-width': this.STROKE
            })
            .hover(function () {
                this.attr('fill', $this.ACTIVE);
            }, function () {
                this.attr('fill', $this.COLOR);
            })
            .toFront();
    },
    /**
     * Установка цвета узла по умолчанию
     */
    setDefault: function () {
        this.COLOR = this.DEFAULT;

        this.el.attr('fill', this.COLOR);
    }
});


/**
 * Класс области
 *
 * @param points - набор узлов
 * @param id - ID области, если он известен
 * @constructor
 */
var Area = function (points, id) {
    this.COLOR_RELATED = '#9F5500';
    this.COLOR_UNRELATED = '#000';
    this.ACTIVE_RELATED = '#BF7500';
    this.ACTIVE_UNRELATED = '#444';

    this.COLOR = this.COLOR_UNRELATED;
    this.ACTIVE = this.ACTIVE_UNRELATED;
    this.OPACITY = 0.5;
    this.STROKE = 2;

    this.checkPointsOrder = {
        h: [2, 1, 3],
        v: [2, 1, 3]
    };

    this.points = points;
    this.related = false;

    this.el = this.draw();

    this.setId(id);

    this.el.markType = painter.TYPE_AREA;
    this.el.related = false;
};

$.extend(Area.prototype, {
    /**
     * Отрисовка области
     *
     * @returns Raphael Object
     */
    draw: function () {
        var $this = this;

        return painter.paper
            .path(this.getPath())
            .attr({
                'fill': this.COLOR,
                'fill-opacity': this.OPACITY,
                'stroke-width': this.STROKE
            })
            .hover(function () {
                if (painter.mode !== painter.MODE_PAINT) {
                    this.attr('fill', $this.ACTIVE);
                }
            }, function () {
                this.attr('fill', $this.COLOR);
            })
            .toBack();
    },
    /**
     * Формирование описания пути из координат узлов области
     *
     * @returns {string}
     */
    getPath: function (points) {
        if (typeof(points) !== 'object') {
            points = this.points;
        }

        var path = '';

        for (var i in points) {
            path += (path === '' ? 'M' : 'L');
            path += points[i].x + ' ' + points[i].y;
        }

        return path + 'z';
    },
    /**
     * Устанавливает ID области
     *
     * @param id
     */
    setId: function (id) {
        this.id = id;
        this.el.markId = id;
    },
    /**
     * Пометить область как отвязанную
     */
    setUnrelated: function () {
        this.related = false;
        this.el.related = false;

        this.removeLabel();

        this.COLOR = this.COLOR_UNRELATED;
        this.ACTIVE = this.ACTIVE_UNRELATED;
        this.el.attr('fill', this.COLOR);
    },
    /**
     * Пометить область как связанную
     *
     * @param text - метка связанного объекта
     */
    setRelated: function (text) {
        this.related = true;
        this.el.related = true;

        this.setLabel(text);

        this.COLOR = this.COLOR_RELATED;
        this.ACTIVE = this.ACTIVE_RELATED;
        this.el.attr('fill', this.COLOR);
    },
    /**
     * Задать подпись для области
     *
     * @param text - текст подписи
     */
    setLabel: function (text) {
        this.label = new MarkLabel(text);

        this.positionLabel();
    },
    /**
     * Удаляет подпись области
     */
    removeLabel: function () {
        if (this.label) {
            this.label.remove();

            this.label = null;
        }
    },
    /**
     * Позиционирование подписи
     */
    positionLabel: function () {
        var checkPoints = this.getCheckPoints();
        var goodPoint = checkPoints[0];
        var areaPath = this.el.attrs.path;

        var labelBBox = this.label.el.getBBox();
        var labelHalfWidth = labelBBox.width / 2;
        var labelHalfHeight = labelBBox.height / 2;

        for (var i in checkPoints) {
            var virtualPoints = [
                {x: checkPoints[i].x - labelHalfWidth, y: checkPoints[i].y - labelHalfHeight},
                {x: checkPoints[i].x + labelHalfWidth, y: checkPoints[i].y - labelHalfHeight},
                {x: checkPoints[i].x + labelHalfWidth, y: checkPoints[i].y + labelHalfHeight},
                {x: checkPoints[i].x - labelHalfWidth, y: checkPoints[i].y + labelHalfHeight}
            ];

            if (!Raphael.pathIntersection(areaPath, this.getPath(virtualPoints)).length) {
                goodPoint = checkPoints[i];

                break;
            }
        }

        this.label.setPosition(goodPoint);
    },
    /**
     * Получение набора точек, подходящих для размещения подписи
     *
     * @returns {Array}
     */
    getCheckPoints: function () {
        var defPoint;
        var points = [];
        var bBox = this.el.getBBox();
        var hPart = Math.round(bBox.width / 3);
        var vPart = Math.round(bBox.height / 3);

        for (var i in this.checkPointsOrder.h) {
            var x = Math.round((bBox.x * 2 + hPart * (this.checkPointsOrder.h[i] - 1) + hPart * this.checkPointsOrder.h[i]) / 2);

            for (var j in this.checkPointsOrder.v) {
                var y = Math.round((bBox.y * 2 + vPart * (this.checkPointsOrder.v[j] - 1) + vPart * this.checkPointsOrder.v[j]) / 2);

                if (typeof(defPoint) === 'undefined') {
                    defPoint = {x: x, y: y};
                }

                if (Raphael.isPointInsidePath(this.el.attrs.path, x, y)) {
                    points.push({x: x, y: y});
                }
            }
        }

        return points || [defPoint];
    },
    getPoints: function () {
        var points = [];

        for (var i in this.points) {
            points.push({
                x: this.points[i].x,
                y: this.points[i].y
            });
        }

        return points;
    },
    /**
     * Возвращает сериализованный массив точек
     */
    serialize: function () {
        var values = this.getPoints();
        return JSON.stringify(values);
    },
    /**
     * Удаление области и ее узлов
     */
    remove: function () {
        for (var i in this.points) {
            this.points[i].el.remove();
        }

        this.el.remove();

        if (this.label) {
            this.label.remove();
        }
    }
});


function showLoader() {
    var html = '<div id="loader"><div class="loader-wrapper"><div id="inTurnFadingTextG"><div id="inTurnFadingTextG_1" class="inTurnFadingTextG">П</div><div id="inTurnFadingTextG_2" class="inTurnFadingTextG">р</div><div id="inTurnFadingTextG_3" class="inTurnFadingTextG">о</div><div id="inTurnFadingTextG_4" class="inTurnFadingTextG">ф</div><div id="inTurnFadingTextG_5" class="inTurnFadingTextG">Б</div><div id="inTurnFadingTextG_6" class="inTurnFadingTextG">р</div><div id="inTurnFadingTextG_7" class="inTurnFadingTextG">о</div><div id="inTurnFadingTextG_8" class="inTurnFadingTextG">н</div><div id="inTurnFadingTextG_9" class="inTurnFadingTextG">ь</div></div></div></div>';
    $('body').append(html);
    return false;
}


function hideLoader() {
    $('#loader').remove();
    return false;
}
