/**
 * @namespace ymaps
 */
ymaps.ready(init);

function init() {
    var myMap = new ymaps.Map("map", {
            // center: [55.76, 37.64], // Москва
            center: [47.25213065, 39.69431600], // Ростов-на-Дону
            zoom: 11
            // Обратите внимание, что в API 2.1 по умолчанию карта создается с элементами управления.
            // Если вам не нужно их добавлять на карту, в ее параметрах передайте пустой массив в поле controls.
            // controls: []
        }, {
            searchControlProvider: 'yandex#search'
        }),


        // Создаем геообъект с типом геометрии "Точка".
        myGeoObject = new ymaps.GeoObject({
            // Описание геометрии.
            geometry: {
                type: "Point",
                coordinates: [47.28, 39.78]
            },
            // Свойства.
            properties: {
                // Контент метки.
                iconContent: 'Эту иконку можно передвигать',
                hintContent: 'Мышкой тащи'
            }
        }, {
            // Опции.
            // Иконка метки будет растягиваться под размер ее содержимого.
            preset: 'islands#blackStretchyIcon',
            // Метку можно перемещать.
            draggable: true
        }),
        myPieChart = new ymaps.Placemark([
            46.28, 39.28
        ], {
            // Данные для построения диаграммы.
            data: [
                {weight: 8, color: '#0E4779'},
                {weight: 6, color: '#1E98FF'},
                {weight: 4, color: '#82CDFF'}
            ],
            iconCaption: "Диаграмма"
        }, {
            // Зададим произвольный макет метки.
            iconLayout: 'default#pieChart',
            // Радиус диаграммы в пикселях.
            iconPieChartRadius: 30,
            // Радиус центральной части макета.
            iconPieChartCoreRadius: 10,
            // Стиль заливки центральной части.
            iconPieChartCoreFillStyle: '#ffffff',
            // Cтиль линий-разделителей секторов и внешней обводки диаграммы.
            iconPieChartStrokeStyle: '#ffffff',
            // Ширина линий-разделителей секторов и внешней обводки диаграммы.
            iconPieChartStrokeWidth: 3,
            // Максимальная ширина подписи метки.
            iconPieChartCaptionMaxWidth: 200
        });

    myMap.geoObjects
        .add(myGeoObject)
        .add(myPieChart)
        .add(new ymaps.Placemark([47.35644155, 39.87895002], {
            balloonContent: 'стиль <strong>с иконкой</strong>'
        }, {
            preset: 'islands#redSportIcon'
        }))
        .add(new ymaps.Placemark([47.22866716, 39.74624682], {
            balloonContent: 'всплывающее <strong>описание</strong>'
        }, {
            preset: 'islands#governmentCircleIcon',
            iconColor: '#3b5998'
        }))
        .add(new ymaps.Placemark([47.20293560, 39.70539141], {
            balloonContent: 'цвет <strong>какой-то цвет</strong>',
            iconCaption: 'Щелкни по иконке...',
            balloonContentBody: [
                '<address>',
                '<strong>Новостройка в Ростове</strong>',
                '<br/>',
                'Адрес: 119021, Ростов-на-Дону, ул. Ленина, 16',
                '<br/>',
                'Подробнее: <a href="https://company.yandex.ru/">https://company.yandex.ru</a>',
                '<br/>',
                '<a class="search-map__item-image" target="_blank" href=""><img src="//bck.superrielt.ru/images/upload/59707641e690e.jpg"></a>',
                '<br/>',
                '<span class="search-map__item-date">Срок сдачи: <b>4 квартал 2018</b></span>',
                '</address>'

            ].join('')
        }, {
            preset: 'islands#greenDotIconWithCaption'
        }))

        // иконка новостройки - пример
        .add(new ymaps.Placemark([47.17180703, 39.64633989], {
            balloonContent: 'цвет <strong>голубой</strong>',
            iconCaption: 'ЖК "Новостройка"',
            balloonContentBody: [
                '<address>',
                '<strong>Офис Яндекса в Москве</strong>',
                '<br/>',
                'Адрес: 119021, Москва, ул. Льва Толстого, 16',
                '<br/>',
                'Подробнее: <a href="https://company.yandex.ru/">https://company.yandex.ru</a>',
                '</address>'
            ].join('')
        }, {
            preset: 'islands#blueCircleDotIconWithCaption',
            iconCaptionMaxWidth: '150'
        }));
}
