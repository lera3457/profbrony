$(function () {
    function updateFlat ($form)
    {
        var flatId = $form.data('flat-id');
        var data = $form.serializeArray();
        showLoader();
        $.ajax({
            url: Routing.generate('admin_app_flat_update_flat', {id: flatId}),
            method: 'POST',
            data: data,
            success: function () {
                hideLoader()
            }
        });
    }
    var $modalConfirm = $('#modalConfirm');

    $('.js-import-form').on('submit', function () {
        showLoader();
    });


    // Реакция на iCheckbox
    $('.js-border-blocks-tabs input')
        .on('ifToggled', function(e){
            updateFlat($(e.currentTarget).closest('form'));
        });

    $('.js-border-blocks-tabs')

        // редактирование подъезда
        .on('click', '.js-update-block', function () {
            var blockId = $(this).closest('a').data('block-id');
            showLoader();
            window.location.href = Routing.generate('admin_app_block_edit', {id: blockId});
        })

        // удаление подъезда
        .on('click', '.js-delete-block', function () {
            var blockId = $(this).closest('a').data('block-id');
            $modalConfirm.modal('show');
            $modalConfirm.find('.js-btn-delete-confirm').attr('href', Routing.generate('admin_app_block_delete_block', {id: blockId}));
            $modalConfirm.on('click', '.js-btn-delete-confirm', function() {
                $modalConfirm.modal('hide');
                showLoader();
            });
        })

        // Добавление этажа
        .on('click', '.js-create-floor', function (e) {
            e.preventDefault();
            var blockId = $(this).data('block-id');
            showLoader();
            $.ajax({
                url: Routing.generate('admin_app_floor_create', {id: blockId}),
                method: 'POST'
            }).then(function () {
                hideLoader();
                window.location.reload()
            });
        })

        //Удаление этажа
        .on('click', '.js-remove-floor', function (e) {
            e.preventDefault();
            var floorId = $(this).data('floor-id');
            $modalConfirm.modal('show');
            $modalConfirm.find('.js-btn-delete-confirm').attr('href', Routing.generate('admin_app_floor_delete_floor', {id: floorId}));
            $modalConfirm.on('click', '.js-btn-delete-confirm', function() {
                $modalConfirm.modal('hide');
                showLoader()
            });
        })

        // Добавление квартиры
        .on('click', '.js-create-flat', function (e) {
            e.preventDefault();

            var $flatModal = $('#flatModal');
            var floorId = $(e.currentTarget).data('floor-id');
            $flatModal.find('#flat_floor').val(floorId);

            $flatModal.modal();
        })

        // Удаленик квартиры
        .on('click', '.js-remove-flat', function (e) {
            e.preventDefault();
            var flatId = $(this).data('flat-id');
            $modalConfirm.modal('show');
            $modalConfirm.find('.js-btn-delete-confirm').attr('href', Routing.generate('admin_app_flat_delete_flat', {id: flatId}));
            $modalConfirm.on('click', '.js-btn-delete-confirm', function() {
                $modalConfirm.modal('hide');
                showLoader();
            });
        })

        // Обновление квартиры
        .on('change', 'form', function() {
            updateFlat($(this))
        })

        // Стрелка у таба
        .on('click', '.js-collapsible', function(e) {
            var $href = $(e.currentTarget);
            var $icon = $href.find('i.fa');
            if ($href.hasClass('collapsed')) {
                $icon.removeClass('fa-angle-down');
                $icon.addClass('fa-angle-up');
            } else {
                $icon.removeClass('fa-angle-up');
                $icon.addClass('fa-angle-down');
            }
        });

//////////////////////////////////////////////////



//........ Работа с разметкой планировки ...............//

    var layoutCanvas = $('#layout-canvas');
    var layoutImage = layoutCanvas.find('img');
    var painterContextMenuLayer = $('#painter-context-menu-layer');

    layoutImage.one('load', function () {
        layoutCanvas.css({
            width: layoutImage.width(),
            height: layoutImage.height(),
            background: 'url(' + layoutImage.attr('src') + ')'
        });
        layoutImage.remove();
        new Painter(layoutCanvas, {
            functionContextMenu: painterContextMenu,
            functionContextMenuHide: painterContextMenuHide
        });
    }).each(function () {
        this.complete && layoutImage.trigger('load');
    });

    function painterContextMenu(coordinates, area) {
        if (area.related) {
            $('.js-painter-context-menu-relate').hide();
            $('.js-painter-context-menu-unrelate').show();
        } else {
            $('.js-painter-context-menu-relate').show();
            $('.js-painter-context-menu-unrelate').hide();
        }

        painterContextMenuLayer
            .css({
                left: coordinates.x + 10,
                top: coordinates.y + 10
            })
            .data('area-id', area.markId)
            .show();
    }

    function painterContextMenuHide() {
        painterContextMenuLayer.hide();
    }

});



function showLoader() {
    var html = '<div id="loader"><div class="loader-wrapper"><div id="inTurnFadingTextG"><div id="inTurnFadingTextG_1" class="inTurnFadingTextG">П</div><div id="inTurnFadingTextG_2" class="inTurnFadingTextG">р</div><div id="inTurnFadingTextG_3" class="inTurnFadingTextG">о</div><div id="inTurnFadingTextG_4" class="inTurnFadingTextG">ф</div><div id="inTurnFadingTextG_5" class="inTurnFadingTextG">Б</div><div id="inTurnFadingTextG_6" class="inTurnFadingTextG">р</div><div id="inTurnFadingTextG_7" class="inTurnFadingTextG">о</div><div id="inTurnFadingTextG_8" class="inTurnFadingTextG">н</div><div id="inTurnFadingTextG_9" class="inTurnFadingTextG">ь</div></div></div></div>';
    $('body').append(html);
    return false;
}


function hideLoader() {
    $('#loader').remove();
    return false;
}


