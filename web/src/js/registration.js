// AdminLTE, skins and font-awesome
require('admin-lte');
require('icheck');

require('font-awesome/less/font-awesome.less');
require('ionicons/dist/css/ionicons.css');
require('icheck/skins/all.css');

$(function () {
    $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
    });
});
